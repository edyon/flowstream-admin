import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

// This sets the mock adapter on the default instance
const mock = new MockAdapter(axios);

// arguments for reply are (status, data, headers)

// Mock any GET request to /users
mock.onGet('/api/current_user').reply(200,
    {data: {_id: 'uisahduiahsd', name: 'John Smith'}}
);