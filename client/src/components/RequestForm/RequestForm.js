import React, {Component} from 'react';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';

import classes from './RequestForm.scss';

class RequestForm extends Component {

    state = {
        requestForm: {
            message: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Write message'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Message'
            }
        },
        formIsValid: false
    };

    submitHandler = (event) => {
        event.preventDefault();

        let saveItem = {};
        for (let formElementIdentifier in this.state.requestForm) {
            saveItem[formElementIdentifier] = this.state.requestForm[formElementIdentifier].value;
        }

        this.props.saveItem(saveItem);
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.requestForm
        };
        const updatedElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedElement.value = event.target.value;
        updatedElement.valid = this.checkValidity(updatedElement.value, updatedElement.validation);
        updatedElement.touched = true;
        updatedForm[inputIdentifier] = updatedElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        this.setState({requestForm: updatedForm, formIsValid: formIsValid});
    };

    render() {
        const formElementsArray = [];
        for (let key in this.state.requestForm) {
            formElementsArray.push({
                id: key,
                config: this.state.requestForm[key]
            });
        }

        const form = (
            <form key="2" onSubmit={this.submitHandler}>
                {formElementsArray.map(formElement => (
                    <Input
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        label={formElement.config.label}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                ))}
                <Button originalType="submit" btnType="Green" disabled={!this.state.formIsValid}>Make request</Button>
                <Button originalType="button" btnType="White" clicked={this.props.cancelItem}>Cancel</Button>
            </form>
        );

        return (
            <div className={classes.RequestForm}>
                {form}
            </div>
        );
    }
}

export default RequestForm;