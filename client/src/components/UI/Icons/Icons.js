import React from 'react';

export const IconCamera = (props) => (
    <svg id="icon-camera" width={props.iconWidth} height={props.iconHeight} viewBox="0 0 512 512" fill={props.fillColor}>
        <path
            d="m448 96l-64 0c0-35-29-64-64-64l-128 0c-35 0-64 29-64 64l-64 0c-35 0-64 29-64 64l0 288c0 35 29 64 64 64l384 0c35 0 64-29 64-64l0-288c0-35-29-64-64-64z m-256-32l128 0c18 0 32 14 32 32l-192 0c0-18 14-32 32-32z m288 384c0 18-14 32-32 32l-384 0c-18 0-32-14-32-32l0-192 64 0 0-32-64 0 0-32 64 0 0-32-64 0c0-18 14-32 32-32l384 0c18 0 32 14 32 32z m-224-256c-71 0-128 57-128 128 0 71 57 128 128 128 71 0 128-57 128-128 0-71-57-128-128-128z m0 224c-53 0-96-43-96-96 0-53 43-96 96-96 53 0 96 43 96 96 0 53-43 96-96 96z"></path>
    </svg>
);

export const IconOptions = (props) => (
    <svg id="icon-camera" width={props.iconWidth} height={props.iconHeight} viewBox="0 0 512 512" fill={props.fillColor}>
        <path d="m64 192c-35 0-64 29-64 64 0 35 29 64 64 64 35 0 64-29 64-64 0-35-29-64-64-64z m0 96c-18 0-32-14-32-32 0-18 14-32 32-32 18 0 32 14 32 32 0 18-14 32-32 32z m192-96c-35 0-64 29-64 64 0 35 29 64 64 64 35 0 64-29 64-64 0-35-29-64-64-64z m0 96c-18 0-32-14-32-32 0-18 14-32 32-32 18 0 32 14 32 32 0 18-14 32-32 32z m192-96c-35 0-64 29-64 64 0 35 29 64 64 64 35 0 64-29 64-64 0-35-29-64-64-64z m0 96c-18 0-32-14-32-32 0-18 14-32 32-32 18 0 32 14 32 32 0 18-14 32-32 32z"></path>
    </svg>
);

export const IconSelectArrow = (props) => (
    <svg id="icon-camera" width={props.iconWidth} height={props.iconHeight} viewBox="0 0 512 512" fill={props.fillColor}>
        <path d="m443 149c-6-6-15-6-21 0l-166 180-166-180c-6-6-15-6-21 0-6 6-6 15 0 21 0 0 175 193 176 193 3 4 7 5 11 5 4 0 8-1 11-5 1 0 176-193 176-193 6-6 6-15 0-21z"></path>
    </svg>
);

