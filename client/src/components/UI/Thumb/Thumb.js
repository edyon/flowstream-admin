import React from 'react';

import classes from './Thumb.scss';

const thumb = (props) => {
    return (
        <div className={classes.Thumb}>
            {props.url ? <img alt="User Thumb" src={props.url}/> : null}
        </div>
    );
};

export default thumb;