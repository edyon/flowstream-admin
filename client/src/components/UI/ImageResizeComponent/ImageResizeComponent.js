import React, {Component} from 'react';
import {Cropper} from 'react-image-cropper';
import classes from './ImageResizeComponent.scss';
import {IconCamera} from '../Icons/Icons';

class ImageResizeComponent extends Component {

    state = {
        scale: 1
    };

    b64toBlob = (b64Data, contentType, sliceSize) => {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var b64DataString = b64Data.substr(b64Data.indexOf(',') + 1);
        var byteCharacters = atob(b64DataString);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {
            type: contentType
        });
        return blob;
    };

    onChange = (image, index) => {
        let imageBase64 = this.cropper[index].crop();
        let blob = this.b64toBlob(imageBase64);
        let file = new File([blob], image.image_title);
        const item = {file: file};
        this.props.onCropImage(item, index);
    };


    render() {
        this.cropper = [];
        if (this.props.imagesArray.length > 0) {
            this.cropperElements =
                this.props.imagesArray.map((image, index) => (
                    <div key={[this.props.Id, 'cropper', index].join(' ')}
                         className={classes.ImageThumb}>
                        <Cropper
                            src={image.image_url}
                            ref={ ref => {
                                this.cropper[index] = ref
                            }}
                            width={330}
                            height={330}
                            allowNewSelection={false}
                            onChange={() => this.onChange(image, index)}
                            onImgLoad={() => this.onChange(image, index)}
                        />
                    </div>
                ))
        }

        return (
            <div className={classes.ImageUploadComponentHolder}>
                {this.cropperElements}
                <div className={classes.UploadImageButtonHolder}
                     onClick={this.props.click}>
                    <div className={classes.UploadImageButtonHolderButton}>
                        <IconCamera className={classes.UploadImageButton}
                                    fillColor="#fff" iconWidth="40px"
                                    iconHeight="40px"/>
                    </div>
                </div>
            </div>
        )
    }
}
;


export default ImageResizeComponent;