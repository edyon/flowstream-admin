import React, {Component} from 'react';
import classes from './Input.scss';
import {IconCamera} from '../Icons/Icons';

class input extends Component {

    invokeFileInput(){
        this.fileInput.click();
    }


    render(){
        let inputElement = null;
        const inputClasses = [classes.InputElement];

        if (this.props.invalid && this.props.shouldValidate && this.props.touched) {
            inputClasses.push(classes.Invalid);
        }

        switch (this.props.elementType) {
            case ( 'image-upload-input' ):
                inputElement =
                    <div className={classes.UploadImageButtonWrapper}>
                        <input
                            ref={(ref) => this.fileInput = ref}
                            className={inputClasses.join(' ')}
                            {...this.props.elementConfig}
                            value={this.props.value}
                            onChange={this.props.changed}
                            multiple/>
                    </div>
                break;
            case ( 'input' ):
                inputElement = <input
                    data-testid={this.props.elementId}
                    className={inputClasses.join(' ')}
                    {...this.props.elementConfig}
                    value={this.props.value}
                    onChange={this.props.changed}/>;
                break;
            case ( 'textarea' ):
                inputElement = <textarea
                    data-testid={this.props.elementId}
                    className={inputClasses.join(' ')}
                    {...this.props.elementConfig}
                    value={this.props.value}
                    onChange={this.props.changed}/>;
                break;
            case ( 'select' ):
            case ( 'categorySelect' ):
            case ( 'subCategorySelect' ):
                let selectClasses = [classes.DropDown, classes.InputElement];
                if (this.props.elementType === 'categorySelect') selectClasses.push(classes.categorySelect);
                if (this.props.elementType === 'subCategorySelect') selectClasses.push(classes.subCategorySelect);
                inputElement = (
                    <select
                        className={selectClasses.join(' ')}
                        placeholder={this.props.elementConfig.placeholder}
                        value={this.props.value}
                        onChange={this.props.changed}>
                        {this.props.elementConfig.options.map(option => (
                            <option key={option.name} value={option.name}>
                                {option.name}
                            </option>
                        ))}
                    </select>
                );

                break;
            case ( 'checkbox' ):
                inputElement = (
                    this.props.elementConfig.options.map((option, index) => (
                        [
                            <input
                                key={[this.props.elementId, 'input', index].join(' ')}
                                type="checkbox"
                                checked={this.props.value.indexOf(option.value.toString()) !== -1}
                                data-testid={this.props.elementId}
                                value={option.value}
                                name={this.props.Id}
                                onChange={this.props.changed}
                                >
                            </input>,
                            <label className={classes.CheckboxLabel} key={[this.props.elementId, 'label', index].join(' ')}>{option.name}</label>
                        ]
                    ))
                );
                break;
            case ( 'radio' ):
                inputElement = (
                    this.props.elementConfig.options.map((option, index) => (
                        [
                            <input
                                key={[this.props.elementId, 'input', index].join(' ')}
                                type="radio"
                                value={option.value}
                                name={this.props.Id}
                                onChange={this.props.changed}
                                checked={ option.value.toString() === this.props.value.toString()}
                            >
                            </input>,
                            <label className={classes.RadioLabel} key={[this.props.elementId, 'label', index].join(' ')}>{option.name}</label>
                        ]
                    ))
                );
                break;
            default:
                inputElement = <input
                    data-testid={this.props.elementId}
                    className={inputClasses.join(' ')}
                    {...this.props.elementConfig}
                    value={this.props.value}
                    onChange={this.props.changed}/>;
        }

        return (

            <div className={[classes.InputHolder, classes[this.props.elementType]].join(' ')}>
                <label className={classes.InputLabel}>{this.props.label}</label>
                {inputElement}
            </div>
        );
    }

};

export default input;