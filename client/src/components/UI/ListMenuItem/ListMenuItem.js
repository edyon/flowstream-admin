import React from 'react';
import classes from './ListMenuItem.scss';

const burgerIngredient = (props) => {
    return (
        <li className={classes.MenuListItem}  onClick={props.onClick}>
            {props.children}
        </li>
    )
};

export default burgerIngredient;