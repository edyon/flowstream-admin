import React, {Component} from 'react';
import {connect} from 'react-redux';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import Backdrop from '../UI/Backdrop/Backdrop';
import Wrapper from '../../hoc/Wrapper/Wrapper';
import ImageResizeComponent from '../UI/ImageResizeComponent/ImageResizeComponent';
import {categories} from '../../models';
import popupClasses from '../../assets/style/Popup.scss';
import classes from './ItemForm.scss';
import * as actions from '../../store/actions/index';


class ItemForm extends Component {

    constructor(props) {
        super(props);
        this.popupClasses = [popupClasses.popupHolder, popupClasses.itemForm];
        this.formElements = [];
    }

    state = {
        itemForm: {
            fileInput: {
                elementType: 'image-upload-input',
                elementConfig: {
                    type: 'file',
                    placeholder: ''
                },
                value: '',
                validation: {},
                valid: true,
                files: null,
                touched: false,
                label: 'Upload'
            },
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Title'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Title'
            },
            description: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Description'
                },
                value: '',
                validation: {
                    required: false
                },
                valid: false,
                touched: false,
                label: 'Description'
            },
            category: {
                elementType: 'categorySelect',
                elementConfig: {
                    type: 'select',
                    options: categories,
                    placeholder: 'Main category'
                },
                validation: {},
                valid: true,
                value: categories[0].name,
                label: 'Category'
            },
            sub_category: {
                elementType: 'subCategorySelect',
                elementConfig: {
                    type: 'select',
                    options: categories[0].sub_categories,
                    placeholder: 'Sub category'
                },
                validation: {},
                valid: true,
                value: categories[0].sub_categories[0].name,
                label: 'Sub category'
            },
            location: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Postal code'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Location'
            },
            shareMethod: {
                elementType: 'radio',
                elementConfig: {
                    type: 'radio',
                    placeholder: '',
                    options: [
                        {name: "Sell", value: 1},
                        {name: "Share for free", value: 2}
                    ]
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Sell or share for free'
            },
            price: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'price'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Price'
            },
            originalPrice: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'What was the original price you paid for it?'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Original price'
            },
            state: {
                elementType: 'select',
                elementConfig: {
                    type: 'select',
                    options: [
                        {name: "As good as new"},
                        {name: "Upholstered/ Upcycled"},
                        {name: "Used but in good condition"},
                        {name: "Alright with some signs of usage"},
                        {name: "Needs work"}
                    ],
                    placeholder: 'State'
                },
                validation: {},
                valid: true,
                value: "",
                label: 'Product state'
            },
            obtainMethod: {
                elementType: 'radio',
                elementConfig: {
                    type: 'radio',
                    placeholder: '',
                    options: [
                        {name: "First come, first served", value: 1},
                        {name: "By request", value: 2}
                    ]
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'How to obtain'
            },
            shipping: {
                elementType: 'checkbox',
                elementConfig: {
                    type: 'checkbox',
                    options: [
                        {name: "Pick Up", value: 1},
                        {name: "Deliver", value: 2}
                    ],
                    placeholder: 'Sub category'
                },
                validation: {},
                valid: true,
                value: '',
                label: 'Shipping options'
            },
            payment: {
                elementType: 'checkbox',
                elementConfig: {
                    type: 'checkbox',
                    options: [
                        {name: "Cash", value: 1},
                        {name: "Bank Transfer", value: 2},
                        {name: "Credit Card", value: 3}
                    ],
                    placeholder: 'Sub category'
                },
                validation: {},
                valid: true,
                value: '',
                label: 'Payment options'
            }


        }
        ,
        formIsValid: false
    }
        ;

    componentDidMount() {
        this.croppedImageFile = [];

        if (this.props.item) {
            this.setFormValues();
        }
    }

    setFormValues() {

        const updatedItemForm = {...this.state.itemForm};
        for (let formElementIdentifier in this.state.itemForm) {
            if (formElementIdentifier !== 'fileInput' && formElementIdentifier !== 'sub_category') {
                updatedItemForm[formElementIdentifier] = this.updateElement(updatedItemForm[formElementIdentifier],
                    this.props.item[formElementIdentifier]);

                if (this.props.item['category'] && formElementIdentifier === 'category') {
                    updatedItemForm['sub_category'] = this.getSubCategories(updatedItemForm, this.props.item['category']);
                    updatedItemForm['sub_category'] = this.updateElement(updatedItemForm['sub_category'], this.props.item['sub_category']);
                }
            }
        }

        this.setState({itemForm: updatedItemForm, formIsValid: true});
    }

    updateElement(element, value) {
        const formElement = {...element};
        formElement.value = value || '';
        formElement.valid = this.checkValidity(value, formElement.validation);
        formElement.touched = false;
        return formElement;
    }

    submitHandler = (event) => {
        event.preventDefault();

        let saveItem = {};
        for (let formElementIdentifier in this.state.itemForm) {
            if (this.state.itemForm[formElementIdentifier].elementConfig.type === 'file') {
                //const files = this.state.itemForm[formElementIdentifier].files;
                saveItem.files = this.croppedImageFile ? this.croppedImageFile : null;
            } else {
                saveItem[formElementIdentifier] = this.state.itemForm[formElementIdentifier].value;
            }
        }

        this.props.saveItem(saveItem);
    };

    showSelectedImages = (files) => {
        this.props.onSelectFiles(files);
    };

    onCropImage = (item, index) => {
        this.croppedImageFile[index] = item.file;
    };

    checkValidity(value, rules) {
        let isValid = true;
        // if (!rules) {
        //     return true;
        // }
        //
        // if (rules.required) {
        //     isValid = value.trim() !== '' && isValid;
        // }
        //
        // if (rules.minLength) {
        //     isValid = value.length >= rules.minLength && isValid
        // }
        //
        // if (rules.maxLength) {
        //     isValid = value.length <= rules.maxLength && isValid
        // }
        //
        // if (rules.isEmail) {
        //     const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        //     isValid = pattern.test(value) && isValid
        // }
        //
        // if (rules.isNumeric) {
        //     const pattern = /^\d+$/;
        //     isValid = pattern.test(value) && isValid
        // }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedItemForm = {
            ...this.state.itemForm
        };
        const updatedFormElement = {
            ...updatedItemForm[inputIdentifier]
        };

        if (updatedFormElement.elementConfig.type === 'file') {
            updatedFormElement.files = event.target.files;
            updatedFormElement.value = event.target.value;
            this.showSelectedImages(event.target.files);
        } else if (updatedFormElement.elementConfig.type === 'checkbox') {
            updatedFormElement.value = this.getSelectedCheckboxes(updatedFormElement, event.target.value, event.target.checked) ;
        } else {
            updatedFormElement.value = event.target.value;
        }

        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedItemForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedItemForm) {
            formIsValid = updatedItemForm[inputIdentifier].valid && formIsValid;
        }

        if (inputIdentifier === 'category') {
            updatedItemForm['sub_category'] = this.getSubCategories(updatedItemForm, event.target.value);
        }

        this.setState({itemForm: updatedItemForm, formIsValid: formIsValid});
    };

    getSelectedCheckboxes (updatedFormElement, value, checked){
        let index = updatedFormElement.value.indexOf(value);
        let newValue = [
            ...updatedFormElement.value
        ];

        if(checked && index === -1){
            //add
            newValue.push(value);
        }else if (!checked && index !== -1){
            // remove
            newValue.splice(index, 1);
        }

        return newValue;
    };

    getSubCategories(updatedItemForm, categoryName) {
        const category = categories.find(item => item.name === categoryName);
        const index = categories.indexOf(category);
        const subCategories = categories[index].sub_categories;
        const subCategoryElement = {
            ...updatedItemForm['sub_category']
        };
        const config = {...subCategoryElement.elementConfig};
        config.options = subCategories;
        subCategoryElement.elementConfig = config;
        subCategoryElement.value = categories[index].sub_categories[0].name;
        return subCategoryElement;
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.itemForm) {
            formElementsArray.push({
                id: key,
                config: this.state.itemForm[key]
            });
        }

        let imageResizer =
            <ImageResizeComponent Id='imageResizeComponent'
                                  className={classes.ImageThumb}
                                  onCropImage={this.onCropImage}
                                  imagesArray={(this.props.item && this.props.item.imagesArray) ? this.props.item.imagesArray : []}
                                  click={() => this.formElements['fileInput'].invokeFileInput()}>
            </ImageResizeComponent>


        const form = [
            <div key="1" className={classes.ImageThumbsHolder}>
                {imageResizer}
            </div>,
            <form key="2" onSubmit={this.submitHandler}>
                {formElementsArray.map(formElement => (
                    <Input
                        ref={(ref) => this.formElements[formElement.id] = ref}
                        key={formElement.id}
                        Id={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        label={formElement.config.label}
                        changed={(event, index) => this.inputChangedHandler(event, formElement.id)}/>
                ))}
                <div className={classes.InputFormFooter}>
                    <Button originalType="button" btnType="Cancel"
                            clicked={this.props.cancelItem}>Cancel</Button>
                    <Button originalType="submit" btnType="Success"
                            disabled={!this.state.formIsValid}>Save
                        Item</Button>
                </div>
            </form>
        ];

        return (
            <Wrapper>
                <Backdrop show={true} clicked={this.props.cancelItem}/>
                <div className={this.popupClasses.join(' ')}>
                    {form}
                </div>
            </Wrapper>

        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onSelectFiles: file => dispatch(actions.selectFiles(file))
    }
};

export default connect(null, mapDispatchToProps)(ItemForm);