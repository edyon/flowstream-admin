import React, { Component } from 'react';
import Button from '../UI/Button/Button';
import classes from './Item.scss';
import { IconOptions } from '../UI/Icons/Icons';
class Item extends Component {


    constructor(props) {
        super(props);
        this.state = {open: false, showMenu: false};

        this.closeMenu = this.closeMenu.bind(this);
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({showMenu: true}, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    closeMenu(event) {

        if (this.dropdownMenu && !this.dropdownMenu.contains(event.target)) {

            this.setState({showMenu: false}, () => {
                document.removeEventListener('click', this.closeMenu);
            });

        }
    }


    render() {
        return (
            <li className={classes.Item}>
                <div className={classes.Image} onClick={() => this.props.clicked(this.props.item)}>
                    <img alt='Product thumb'
                         src={this.props.item.imagesArray ? this.props.item.imagesArray[0].image_url : ''}/>
                </div>
                <span className={classes.Title}>{this.props.item.title}</span>
                <div className={classes.Location}
                     title={this.props.item.location}>{this.props.item.location}</div>
                {this.props.edit ? (
                        <div>
                            <div className={classes.ItemIconOptions} onClick={(event) => this.showMenu(event)}>
                                <IconOptions  fillColor="#3c4042" iconWidth="24px" iconHeight="24px"/>
                            </div>
                            {
                                this.state.showMenu
                                    ? (
                                        <div className={classes.AccountDropDown}
                                             ref={(element) => {
                                                 this.dropdownMenu = element;
                                             }}>
                                            <ul>
                                                <Button
                                                    clicked={() => this.props.editItem(this.props.item._id)}
                                                    btnType="Success">Edit</Button>
                                                <Button
                                                    clicked={() => this.props.deleteItem(this.props.item._id)}
                                                    btnType="Danger">Delete</Button>
                                            </ul>
                                        </div>
                                    ) : ( null )
                            }
                        </div>) : null}
            </li>
        )
    }
}
;

export default Item;