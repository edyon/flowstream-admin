export const status = {
    open: "Open",
    paused: "Pauzed",
    closed: "Closed",
};

export const categories = [
    {
        name: "Antiques & collectables",
        sub_categories: [
            {name: "Advertising"},
            {name: "Alcohol related"},
            {name: "Art deco & retro"},
            {name: "Automotive & transport"},
            {name: "Banknotes"},
            {name: "Bottles"},
            {name: "Clocks & scientific instruments"},
            {name: "Coins"},
            {name: "Cultural & ethnic"},
            {name: "Documents & maps"},
            {name: "Flags"},
            {name: "Food & drink"},
            {name: "Furniture & woodenware"},
            {name: "Jewellery"},
            {name: "Militaria"},
            {name: "Movie & TV memorabilia"},
            {name: "Museum pieces & artifacts"},
            {name: "New Zealand & Maori"},
            {name: "Ornaments & figurines"},
            {name: "Phonecards"},
            {name: "Pins, badges & patches "},
            {name: "Postcards & writing"},
            {name: "Royal family"},
            {name: "Silver, metalware & tins"},
            {name: "Stamps"},
            {name: "Textiles & linen"},
            {name: "Tools"},
            {name: "Other"}
        ]
    },
    {
        name: "Art",
        sub_categories: [
            {name: "Art supplies & equipment"},
            {name: "Carvings & sculpture"},
            {name: "Drawings"},
            {name: "Hanging sculptures"},
            {name: "Paintings"},
            {name: "Prints"},
            {name: "Tattoos"},
            {name: "Other"}
        ]
    },
    {
        name: "Baby gear",
        sub_categories: [
            {name: "Baby room & furniture"},
            {name: "Backpacks & carriers"},
            {name: "Bath time"},
            {name: "Blankets & covers"},
            {name: "Bouncers & jolly jumpers"},
            {name: "Car seats"},
            {name: "Clothing"},
            {name: "Cots & bassinets"},
            {name: "Feeding"},
            {name: "Monitors"},
            {name: "Nappies & changing"},
            {name: "Prams & strollers"},
            {name: "Safety"},
            {name: "Sleep aids"},
            {name: "Teething necklaces"},
            {name: "Toys"},
            {name: "Walkers"},
            {name: "Other"}
        ]
    },
    {
        name: "Books",
        sub_categories: [
            {name: "Audio books"},
            {name: "Bulk"},
            {name: "Children & babies"},
            {name: "Comics & graphic novels"},
            {name: "Fiction & literature"},
            {name: "Foreign language"},
            {name: "Magazines"},
            {name: "Non-fiction"},
            {name: "Rare & collectable"},
            {name: "Teaching resources & education"},
            {name: "Young adult fiction"},
            {name: "Other"}
        ]
    },
    {
        name: "Building & renovation",
        sub_categories: [
            {name: "Bathroom"},
            {name: "Building supplies"},
            {name: "Carpet, tiles & flooring"},
            {name: "Doors, windows & mouldings"},
            {name: "Electrical & lighting"},
            {name: "Fixtures & fittings"},
            {name: "Heating & cooling"},
            {name: "Kitchen"},
            {name: "Painting & wallpaper"},
            {name: "Plumbing & gas"},
            {name: "Portable buildings"},
            {name: "Tools"},
            {name: "Other"}

        ]
    },
    {
        name: "Business, farming & industry",
        sub_categories: [
            {name: "Businesses for sale"},
            {name: "Carbon credits"},
            {name: "Farming & forestry"},
            {name: "Industrial"},
            {name: "Office equipment & supplies"},
            {name: "Office furniture"},
            {name: "Postage & packing supplies"},
            {name: "Retail & hospitality"},
            {name: "Wholesale lots"},
            {name: "Other"}
        ]
    },
    {
        name: "Clothing & Fashion",
        sub_categories: [
            {name: "Boys"},
            {name: "Girls"},
            {name: "Men"},
            {name: "Women"},
            {name: "Other"}
        ]
    },
    {
        name: "Computers",
        sub_categories: [
            {name: "Blank discs & cases"},
            {name: "Cables & adaptors"},
            {name: "Components"},
            {name: "Computer furniture"},
            {name: "Desktops"},
            {name: "Domain names"},
            {name: "External storage"},
            {name: "Laptops"},
            {name: "Monitors"},
            {name: "Networking & modems"},
            {name: "Peripherals"},
            {name: "Printer accessories & supplies"},
            {name: "Printers"},
            {name: "Servers"},
            {name: "Software"},
            {name: "Tablets & E-book readers"},
            {name: "Vintage"},
            {name: "Other"}
        ]
    },
    {
        name: "Crafts",
        sub_categories: [
            {name: "Cake design"},
            {name: "Candle making"},
            {name: "Crochet"},
            {name: "Cross-stitch"},
            {name: "Embroidery"},
            {name: "Fabric"},
            {name: "Greeting cards & envelopes"},
            {name: "Jewellery making & beading"},
            {name: "Knitting & Weaving"},
            {name: "Mosaics"},
            {name: "Painting & drawing"},
            {name: "Pottery & ceramics"},
            {name: "Quilting"},
            {name: "Scrapbooking"},
            {name: "Sewing"},
            {name: "Spinning"},
            {name: "Stamping"},
            {name: "Transfers"},
            {name: "Woodcraft"},
            {name: "Other"}
        ]
    },
    {
        name: "Electronics & photography",
        sub_categories: [
            {name: "Batteries"},
            {name: "Binoculars & telescopes"},
            {name: "Camera accessories"},
            {name: "Digital cameras"},
            {name: "DVD & Blu-ray players"},
            {name: "Film cameras"},
            {name: "GPS"},
            {name: "Home audio"},
            {name: "iPod & MP3 accessories"},
            {name: "iPods"},
            {name: "Media streaming"},
            {name: "Memory cards"},
            {name: "MP3 players"},
            {name: "Phone & fax"},
            {name: "Projectors & screens"},
            {name: "Radio equipment"},
            {name: "TVs"},
            {name: "Video cameras"},
            {name: "Other"}
        ]
    },
    {
        name: "Gaming",
        sub_categories: [
            {name: "Arcade & pinball machines"},
            {name: "Joysticks & gamepads"},
            {name: "Nintendo 3DS"},
            {name: "Nintendo DS"},
            {name: "Nintendo Switch"},
            {name: "Nintendo Wii"},
            {name: "Nintendo Wii U"},
            {name: "PC games"},
            {name: "PlayStation"},
            {name: "PlayStation 2"},
            {name: "PlayStation 3"},
            {name: "PlayStation 4"},
            {name: "PlayStation Vita"},
            {name: "PSP"},
            {name: "Sega"},
            {name: "Trading cards"},
            {name: "War games"},
            {name: "Xbox"},
            {name: "Xbox 360"},
            {name: "Xbox One"},
            {name: "Other"}
        ]
    },
    {
        name: "Health & beauty",
        sub_categories: [
            {name: "Bath & shower"},
            {name: "Body moisturisers"},
            {name: "Face care"},
            {name: "Fragrance gift sets"},
            {name: "Fragrance miniatures"},
            {name: "Gift packs"},
            {name: "Glasses & contacts"},
            {name: "Hair accessories"},
            {name: "Hair care products"},
            {name: "Hair dryers"},
            {name: "Hair straighteners"},
            {name: "Hand & foot care"},
            {name: "Makeup"},
            {name: "Massage"},
            {name: "Medical supplies"},
            {name: "Medicines"},
            {name: "Men's fragrances"},
            {name: "Mobility aids"},
            {name: "Naturopathy"},
            {name: "Personal hygiene"},
            {name: "Pregnancy & maternity"},
            {name: "Relaxation & hypnosis"},
            {name: "Scissors & clippers"},
            {name: "Shaving & hair removal"},
            {name: "Sun care & tanning"},
            {name: "Weight loss"},
            {name: "Women's fragrances"},
            {name: "Other"}
        ]
    },
    {
        name: "Home & living",
        sub_categories: [
            {name: "Bathroom"},
            {name: "Bedding & towels"},
            {name: "Bedroom furniture"},
            {name: "Beds"},
            {name: "Cleaning & bins"},
            {name: "Curtains & blinds"},
            {name: "Food & beverage"},
            {name: "Heating & cooling"},
            {name: "Home décor"},
            {name: "Kitchen"},
            {name: "Lamps"},
            {name: "Laundry"},
            {name: "Lifestyle"},
            {name: "Lounge, dining & hall"},
            {name: "Luggage & travel accessories"},
            {name: "Outdoor, garden & conservatory"},
            {name: "Party & festive supplies"},
            {name: "Security, locks & alarms"},
            {name: "Wine"},
            {name: "Other"}
        ]
    },
    {
        name: "Jewellery & watches",
        sub_categories: [
            {name: "Body jewellery & piercings"},
            {name: "Bracelets & bangles"},
            {name: "Brooches"},
            {name: "Earrings"},
            {name: "Gemstones"},
            {name: "Jewellery boxes"},
            {name: "Matching sets"},
            {name: "Men's jewellery"},
            {name: "Necklaces & pendants"},
            {name: "Rings"},
            {name: "Watches"},
            {name: "Other"}
        ]
    },
    {
        name: "Mobile phones",
        sub_categories: [
            {name: "Accessories"},
            {name: "Mobile phones"},
            {name: "Replacement parts & components"},
            {name: "SIM cards"},
            {name: "Other"}
        ]
    },
    {
        name: "Movies & TV",
        sub_categories: [
            {name: "Blu-ray"},
            {name: "Bulk lots"},
            {name: "DVDs"},
            {name: "VHS"},
            {name: "Other"}
        ]
    },
    {
        name: "Music & instruments",
        sub_categories: [
            {name: "CDs"},
            {name: "Instruments"},
            {name: "Music DVDs"},
            {name: "Music memorabilia"},
            {name: "Music tuition"},
            {name: "PA, pro audio & DJ equipment"},
            {name: "Sheet music"},
            {name: "Tapes"},
            {name: "Vinyl"},
            {name: "Other"}
        ]
    },
    {
        name: "Pets & animals",
        sub_categories: [
            {name: "Birds"},
            {name: "Cats"},
            {name: "Dogs"},
            {name: "Fish"},
            {name: "Lost & found"},
            {name: "Mice & rodents"},
            {name: "Rabbits & Guinea pigs"},
            {name: "Reptiles & turtles"},
            {name: "Other"}
        ]
    },
    {
        name: "Pottery & glass",
        sub_categories: [
            {name: "Glass & crystal"},
            {name: "Porcelain & pottery"},
            {name: "Other"}
        ]
    },
    {
        name: "Services",
        sub_categories: [
            {name: "Domestic services"},
            {name: "Events & entertainment"},
            {name: "Health & wellbeing"},
            {name: "Trades"},
            {name: "Other"}
        ]
    },
    {
        name: "Sports",
        sub_categories: [
            {name: "Aviation"},
            {name: "Basketball"},
            {name: "Bowls & bowling"},
            {name: "Camping & outdoors"},
            {name: "Cricket"},
            {name: "Cycling"},
            {name: "Dancing & gymnastics"},
            {name: "Darts"},
            {name: "Equestrian"},
            {name: "Exercise equipment & weights"},
            {name: "Fishing"},
            {name: "Golf"},
            {name: "Gym memberships"},
            {name: "Hockey"},
            {name: "Hunting & shooting"},
            {name: "Kayaks & canoes"},
            {name: "Kites & kitesurfing"},
            {name: "Martial arts & boxing"},
            {name: "Netball"},
            {name: "Paintball"},
            {name: "Protective gear & armour"},
            {name: "Racquet sports"},
            {name: "Rugby & league"},
            {name: "Running, track & field"},
            {name: "SCUBA & snorkelling"},
            {name: "Skateboarding & rollerblading"},
            {name: "Ski & board"},
            {name: "Snooker & pool"},
            {name: "Softball & baseball"},
            {name: "Sports bags"},
            {name: "Sports memorabilia"},
            {name: "Sports nutrition & supplements"},
            {name: "Surfing"},
            {name: "Swimming"},
            {name: "Trading cards"},
            {name: "Waterskiing & wakeboarding"},
            {name: "Wetsuits & lifejackets"},
            {name: "Windsurfing"},
            {name: "Other"}
        ]
    },
    {
        name: "Toys & models",
        sub_categories: [
            {name: "Bath toys"},
            {name: "Battery & wind-up"},
            {name: "Bears"},
            {name: "Bulk lots"},
            {name: "Die casts"},
            {name: "Dolls"},
            {name: "Educational toys"},
            {name: "Fidget toys"},
            {name: "Figurines & miniatures"},
            {name: "Games, puzzles & tricks"},
            {name: "Kids' arts & crafts"},
            {name: "Kids' furniture"},
            {name: "Lego & building toys"},
            {name: "Models"},
            {name: "Outdoor toys & trampolines"},
            {name: "Play instruments & microphones"},
            {name: "Pretend playing"},
            {name: "Radio control & robots"},
            {name: "Ride-on toys"},
            {name: "Slot cars & tracks"},
            {name: "Soft toys & bears"},
            {name: "Vehicle toys"},
            {name: "Vintage"},
            {name: "Wooden"},
            {name: "Other"}
        ]
    },
    {
        name: "Travel, events & activities",
        sub_categories: [
            {name: "Accommodation"},
            {name: "Activities"},
            {name: "Event tickets"},
            {name: "Flights"},
            {name: "Holiday packages"},
            {name: "Other"}
        ]
    },
    {
        name: "Motors",
        sub_categories: [
            {name: "Aircraft"},
            {name: "Boats & marine"},
            {name: "Buses"},
            {name: "Car parts & accessories"},
            {name: "Car stereos"},
            {name: "Caravans & motorhomes"},
            {name: "Cars"},
            {name: "Horse floats"},
            {name: "Motorbikes"},
            {name: "Specialist cars"},
            {name: "Trailers"},
            {name: "Trucks"},
            {name: "Wrecked cars"},
            {name: "Other"}
        ]
    }
];

