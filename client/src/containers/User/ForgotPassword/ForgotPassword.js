import React, {Component} from 'react';
import {connect} from 'react-redux';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './ForgotPassword.scss';
import * as actions from '../../../store/actions';

class ForgotPassword extends Component {
    state = {
        forgotPasswordForm: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'E-mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                label: 'E-mail'
            }
        },
        formIsValid: false
    };

    submitHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.forgotPasswordForm) {
            formData[formElementIdentifier] = this.state.forgotPasswordForm[formElementIdentifier].value;
        }
        const user = {
            userData: formData
        }

        this.props.onForgotPasswordUser(user);
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedForgotPasswordForm = {
            ...this.state.forgotPasswordForm
        };
        const updatedFormElement = {
            ...updatedForgotPasswordForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedForgotPasswordForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForgotPasswordForm) {
            formIsValid = updatedForgotPasswordForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({
            forgotPasswordForm: updatedForgotPasswordForm,
            formIsValid: formIsValid
        });
    };

    render() {

        let forgotPasswordPage = null;

        if (this.props.auth.user_request_reset_password) {
            forgotPasswordPage =
                <p>An email has been send with instructions to reset yuor
                    password</p>;
        } else {
            const formElementsArray = [];
            for (let key in this.state.forgotPasswordForm) {
                formElementsArray.push({
                    id: key,
                    config: this.state.forgotPasswordForm[key]
                });
            }
            forgotPasswordPage = [
                <div key="1" className={classes.ContactData}>
                    <h2>What's your email?</h2>
                    <h4>Please verify your email for us. Once you do, we'll send
                        instructions to reset your password.</h4>
                    <form onSubmit={this.submitHandler}>
                        {formElementsArray.map(formElement => (
                            <Input
                                key={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                shouldValidate={formElement.config.validation}
                                touched={formElement.config.touched}
                                label={formElement.config.label}
                                changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                        ))}
                        <Button btnType="Success"
                                disabled={!this.state.formIsValid}>Reset
                            Password</Button>
                    </form>
                </div>
            ]
        }

        if (this.props.loading) {
            forgotPasswordPage = <Spinner />;
        }

        return (
            <div>
            {forgotPasswordPage}
            </div>
        );
    }
};

const mapStateToProps = ({auth}) => {
    return {
        auth
    };
};

export default connect(mapStateToProps, actions)(ForgotPassword);