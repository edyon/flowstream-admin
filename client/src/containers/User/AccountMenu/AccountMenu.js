import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import Wrapper from '../../../hoc/Wrapper/Wrapper';
import Payments from '../../Payments/Payments';
import classes from './AccountMenu.scss';


class AccountMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {open: false, showMenu: false};

        this.closeMenu = this.closeMenu.bind(this);
    }

    showMenu(event) {
        event.preventDefault();

        this.setState({showMenu: true}, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }

    closeMenu(event) {

        if (!this.dropdownMenu.contains(event.target)) {

            this.setState({showMenu: false}, () => {
                document.removeEventListener('click', this.closeMenu);
            });

        }
    }


    render() {
        return (
            <Wrapper>
                <div>
                    <div onClick={(event) => this.showMenu(event)}>
                        <span>Account</span>
                    </div>
                    {
                        this.state.showMenu
                            ? (
                                <div className={classes.AccountDropDown}
                                     ref={(element) => {
                                         this.dropdownMenu = element;
                                     }}>
                                    <ul>
                                        <li><Link to='/sharedItems'>
                                            My items
                                        </Link></li>
                                        <li><Payments/></li>
                                        <li>
                                            Credits: {this.props.auth.userData.credits}</li>
                                        <li><a href="/api/logout">Logout</a></li>
                                    </ul>
                                </div>
                            ) : ( null )
                    }
                </div>
            </Wrapper>

        )
    }
}
;

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};


export default connect(mapStateToProps)(AccountMenu);