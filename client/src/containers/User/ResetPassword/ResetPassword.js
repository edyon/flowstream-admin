import React, {Component} from 'react';
import {connect} from 'react-redux';
import qs from 'querystring';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './ResetPassword.scss';
import * as actions from '../../../store/actions';

class ResetPassword extends Component {

    state = {
        resetPasswordForm: {
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'password'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: false
                },
                valid: false,
                touched: false,
                label: 'Password'
            },
            passwordConfirm: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'password'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: false
                },
                valid: false,
                touched: false,
                label: 'Password'
            }
        },
        formIsValid: false
    };

    constructor(props) {
        super(props);
        this.parsedToken = qs.parse(this.props.location.search).hash;
    }

    submitHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.resetPasswordForm) {
            formData[formElementIdentifier] = this.state.resetPasswordForm[formElementIdentifier].value;
        }
        const oPassword = {
            passwordData: formData,
            token: this.parsedToken
        };
        this.props.onResetPasswordUser(oPassword);
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    };

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedResetPasswordForm = {
            ...this.state.resetPasswordForm
        };
        const updatedFormElement = {
            ...updatedResetPasswordForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedResetPasswordForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedResetPasswordForm) {
            formIsValid = updatedResetPasswordForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({
            resetPasswordForm: updatedResetPasswordForm,
            formIsValid: formIsValid
        });
    };

    render() {
        let resetPasswordPage = null;

        if (this.props.auth.user_reset_password_finished) {
            resetPasswordPage =
                <p>Your password has been changed.</p>;
        } else {
            const formElementsArray = [];
            for (let key in this.state.resetPasswordForm) {
                formElementsArray.push({
                    id: key,
                    config: this.state.resetPasswordForm[key]
                });
            }
            resetPasswordPage = [
                <div className={classes.ContactData}>
                    <h2>Change your password</h2>
                    <h4>Please type in your new password</h4>
                    <form onSubmit={this.submitHandler}>
                        {formElementsArray.map(formElement => (
                            <Input
                                key={formElement.id}
                                elementType={formElement.config.elementType}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                shouldValidate={formElement.config.validation}
                                touched={formElement.config.touched}
                                label={formElement.config.label}
                                changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                        ))}
                        <Button btnType="Success"
                                disabled={!this.state.formIsValid}>Reset
                            Password</Button>
                    </form>
                </div>
            ]
        }

        if (this.props.loading) {
            resetPasswordPage = <Spinner />;
        }

        return (
            <div>
                {resetPasswordPage}
            </div>
        );
    }
}

const mapStateToProps =({auth}) => (
    { auth }
);

export default connect(mapStateToProps, actions)(ResetPassword);