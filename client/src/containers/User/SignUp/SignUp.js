import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './SignUp.scss';
import * as actions from '../../../store/actions';

class SignUp extends Component {

    state = {
        registerForm: {
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Username'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'Username'
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'E-mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                label: 'E-mail'
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'password'
            }
        },
        formIsValid: false
    };

    submitHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.registerForm) {
            formData[formElementIdentifier] = this.state.registerForm[formElementIdentifier].value;
        }
        const user = {
            userData: formData
        };

        this.props.onSignUpUser(user);
    };

    checkValidity(value, rules) {
        let isValid = true;
        // if (!rules) {
        //     return true;
        // }
        //
        // if (rules.required) {
        //     isValid = value.trim() !== '' && isValid;
        // }
        //
        // if (rules.minLength) {
        //     isValid = value.length >= rules.minLength && isValid
        // }
        //
        // if (rules.maxLength) {
        //     isValid = value.length <= rules.maxLength && isValid
        // }
        //
        // if (rules.isEmail) {
        //     const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        //     isValid = pattern.test(value) && isValid
        // }
        //
        // if (rules.isNumeric) {
        //     const pattern = /^\d+$/;
        //     isValid = pattern.test(value) && isValid
        // }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedUserForm = {
            ...this.state.registerForm
        };
        const updatedFormElement = {
            ...updatedUserForm[inputIdentifier]
        };

        if (updatedFormElement.elementConfig.type === 'checkbox') {
            updatedFormElement.checked = event.target.checked;
        } else {
            updatedFormElement.value = event.target.value;
        }

        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedUserForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedUserForm) {
            formIsValid = updatedUserForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({
            registerForm: updatedUserForm,
            formIsValid: formIsValid
        });
    };

    redirectOnSignUp = () => {
        if (this.props.auth.user_signed_up) {
            return <Redirect to="/verifyUser"/>
        } else {
            return null;
        }
    };

    render() {
        const formElementsArray = [];
        for (let key in this.state.registerForm) {
            formElementsArray.push({
                id: key,
                config: this.state.registerForm[key]
            });
        }
        let form = (
            <form>
                {formElementsArray.map(formElement => (
                    <Input
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        label={formElement.config.label}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                ))}
                <Button clicked={this.submitHandler} btnType="Success" disabled={!this.state.formIsValid}>Save User</Button>
            </form>
        );
        if (this.props.loading) {
            form = <Spinner/>;
        }
        return (
            <div className={classes.ContactData}>
                {this.redirectOnSignUp()}
                <h4>Register New User</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = ({auth}) => {
    return {
        auth
    }
};


export default connect(mapStateToProps, actions)(SignUp);