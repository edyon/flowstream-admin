import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom';
import UserSignUp from './SignUp/SignUp';
import UserSignIn from './SignIn/SignIn';
import ForgotPassword from './ForgotPassword/ForgotPassword';
import ResetPassword from './ResetPassword/ResetPassword';

class User extends Component {
    render() {
        return (
            <Switch>
                <Route path="/user/signup" component={UserSignUp}/>,
                <Route path="/user/signin" component={UserSignIn}/>,
                <Route path="/user/forgotPassword" component={ForgotPassword}/>
                <Route path="/user/resetPassword/" component={ResetPassword}/>
            </Switch>

        )
    }
}

export default User;