import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from './SignIn.scss';
import * as actions from '../../../store/actions';

export class SignIn extends Component {

    state = {
        signinForm: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'E-mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                label: 'E-mail'
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                label: 'password'
            }
        },
        formIsValid: false
    };

    submitHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.signinForm) {
            formData[formElementIdentifier] = this.state.signinForm[formElementIdentifier].value;
        }
        const user = {
            userData: formData
        }

        this.props.onSignInUser(user);
    };

    checkValidity(value, rules) {
        let isValid = true;
        // if (!rules) {
        //     return true;
        // }
        //
        // if (rules.required) {
        //     isValid = value.trim() !== '' && isValid;
        // }
        //
        // if (rules.minLength) {
        //     isValid = value.length >= rules.minLength && isValid
        // }
        //
        // if (rules.maxLength) {
        //     isValid = value.length <= rules.maxLength && isValid
        // }
        //
        // if (rules.isEmail) {
        //     const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        //     isValid = pattern.test(value) && isValid
        // }
        //
        // if (rules.isNumeric) {
        //     const pattern = /^\d+$/;
        //     isValid = pattern.test(value) && isValid
        // }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedUserForm = {
            ...this.state.signinForm
        };
        const updatedFormElement = {
            ...updatedUserForm[inputIdentifier]
        };

        if (updatedFormElement.elementConfig.type === 'checkbox') {
            console.log(' target = checked' + event.target.checked);
            updatedFormElement.checked = event.target.checked;
        } else {
            console.log(' target = value' + event.target.value);
            updatedFormElement.value = event.target.value;
        }

        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedUserForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedUserForm) {
            formIsValid = updatedUserForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({signinForm: updatedUserForm, formIsValid: formIsValid});
    };

    render() {

        let form;

        if(this.props.user_signed_in){
            form = <Redirect to="/items"/>
        }else if (this.props.loading) {
            form = <Spinner />;
        }else{
            const formElementsArray = [];
            for (let key in this.state.signinForm) {
                formElementsArray.push({
                    id: key,
                    config: this.state.signinForm[key]
                });
            }
            form = [
                <h3 key="1" className={classes.SignInTitle}>Sign In</h3>,
                <form key="2">
                    {formElementsArray.map(formElement => (
                        <Input
                            key={formElement.id}
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            invalid={!formElement.config.valid}
                            shouldValidate={formElement.config.validation}
                            touched={formElement.config.touched}
                            label={formElement.config.label}
                            changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))}
                    <Link to="/user/forgotPassword">Forgot Password</Link>
                    <Button clicked={this.submitHandler}  btnType="Success" disabled={!this.state.formIsValid}>Login</Button>
                </form>
            ]
        }

        return (
            <div className={classes.ContactData}>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_signed_in: state.auth.user_signed_in
    };
};

export default connect(mapStateToProps, actions)(SignIn);