import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import * as actions from '../../store/actions';
import AccountMenu from '../../containers/User/AccountMenu/AccountMenu';
import classes from './Header.scss';
import cyclyLogo from '../../assets/img/cycly-logo.gif';

class Header extends Component {

    renderContent() {
        switch (this.props.auth.userData) {
            case null:
                return [
                    <ul key="1">
                        <li>
                            <Link to='/user/signup'>
                                Register
                            </Link>
                        </li>
                        <li>
                            <Link to='/user/signin'>
                                Login
                            </Link>
                        </li>
                        <li><a href="/auth/google">Login with Google</a>
                        </li>
                    </ul>

                ];
            default:
                return [
                    <Link key="4" className={classes.HeaderShareLink} to='/items/new'>
                        Place item
                    </Link>,
                    <ul key="5" className={classes.HorizontalMenu}>
                        <li>
                            <Link to='/items'>
                                Earn credits
                            </Link>
                        </li>
                        <li>
                            <Link to='/items/new'>
                                Help
                            </Link>
                        </li>
                        <li>
                            <span>
                                Notifications
                            </span>
                        </li>
                        <li>
                            <AccountMenu className={classes.verticalMenu}/>
                        </li>

                    </ul>
                ]
        }
    }

    render() {
        return (
            <nav className={classes.headerContainer}>
                <div className={classes.navWrapper}>
                    <Link to='/items' className={classes.BrandLogo}>
                        <img
                            src={cyclyLogo}
                            alt="Cycly's logo"
                        />
                    </Link>
                    <ul className="right">
                        {this.renderContent()}
                    </ul>
                </div>
            </nav>
        )
    }
}

function mapStateToProps({auth}) {
    return {auth}
}

export default connect(mapStateToProps, actions)(Header);