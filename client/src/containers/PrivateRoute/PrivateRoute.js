import React, { Component } from 'react';
import { Route, Redirect} from 'react-router-dom';

const PrivateRoute = ({component: Component, authenticated, ...rest}) => {
    return (
        <Route
            {...rest}
            render={(props) => authenticated === true
                ? <Component {...props} />
                : <Redirect to='/items'/>}
        />
    )
};

export default PrivateRoute;