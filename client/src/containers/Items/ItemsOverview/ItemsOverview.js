import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Route} from 'react-router-dom';
import Item from '../../../components/Item/Item';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Modal from '../../../components/UI/Modal/Modal';
import ItemShare from '../ItemShare/ItemShare';
import ItemNew from '../ItemNew/ItemNew';
import ItemEdit from '../ItemEdit/ItemEdit';

import * as actions from '../../../store/actions/index';

import classes from './ItemsOverview.scss';

class ItemsOverview extends Component {

    state = {
        item: null
    };

    componentDidMount() {
        this.props.getAllItems();
    }

    editItemHandler = (id) => {
        this.props.history.push(`/items/edit/${id}`);
    };

    deleteItemHandler = (id) => {
        // TODO confirmation popup
        this.props.onDeleteItem(id);
    };

    openItemHandler = (item) => {
        this.setState({item: item});
    };

    closeItemHandler = () => {
        this.setState({item: null});
    };

    render() {
        let items = null;
        if (this.props.loading) {
            items = <Spinner/>
        }

        if (this.props.error && this.props.error.message) {
            items = <span
                className={classes.Error}>{this.props.error.message}</span>
        }

        if (this.props.items) {

            if (this.props.length === 0) {
                items = <span>no items</span>;
            } else {
                items = (
                    <ul>
                        {this.props.items.map(item => (
                            <Item key={item._id} item={item}
                                  edit={(this.props.userData !== null && item._user._id === this.props.userData._id) ? true : false}
                                  editItem={this.editItemHandler}
                                  deleteItem={this.deleteItemHandler}
                                  clicked={this.openItemHandler}
                            />
                        ))}
                    </ul>
                );
            }
        }

        let openedItem = null;
        if (this.state.item) {
            openedItem = (
                <ItemShare item={this.state.item}/>
            );
        }
        return (
            <div className={classes.Overview}>
                <Modal show={this.state.item}
                       modalClosed={this.closeItemHandler}>
                    {openedItem}
                </Modal>
                {items}
                <Route authenticated={(!this.props.auth) ? false : true }
                       path='/items/new' exact component={ItemNew}/>
                <Route authenticated={(!this.props.auth) ? false : true }
                       path='/items/edit/:id' exact component={ItemEdit}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        items: state.items.items,
        loading: state.items.loading,
        error: state.items.error,
        userData: state.auth.userData
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getAllItems: () => dispatch(actions.getAllItems()),
        onDeleteItem: (id) => dispatch(actions.deleteItem(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsOverview);