import React, {Component} from 'react';
import {connect} from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import Thumb from '../../../components/UI/Thumb/Thumb';
import RequestForm from '../../../components/RequestForm/RequestForm';

import * as actions from '../../../store/actions/index';

import classes from './ItemShare.scss';

class ItemShare extends Component {

    state = {
        requesting: false
    };

    componentDidMount() {
        this.props.onInitRequestItem();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.requested) {
            this.props.item.requests.push(nextProps.request);
            this.setState({requesting: false});
        }
    }

    requestItemHandler = () => {
        this.setState({requesting: true});
    };

    cancelRequestHandler = () => {
        this.setState({requesting: false});
    };

    saveRequestHandler = (requestItem) => {
        this.props.onRequestItem(this.props.item._id, requestItem);
    };

    render() {
        let requests = (<span>0 <label>Requests</label></span>);
        let requestButton = (
            <div className={classes.Request_Button}>
                <Button originalType="button" btnType="Green"
                        clicked={this.requestItemHandler}>Request</Button>
            </div>

        );

        if (this.props.item.requests) {
            if (this.props.userData) {
                const requests = this.props.item.requests;
                const userRequest = requests.find(item => item._user === this.props.userData._id);
                if (userRequest) { // current user already did a request
                    requestButton = null;
                }
            }

            if (this.props.item.requests.length === 1) {
                requests = (
                    <span className={classes.Requests_Label}>1 <label>Request</label></span>
                );
            } else {
                requests = (
                    <span className={classes.Requests_Label}>{this.props.item.requests.length}
                        <label>Requests</label></span>
                );
            }
        }

        let itemRequest = null;
        if (this.props.item) {
            itemRequest = (
                <div className={classes.Request}>
                    <div className={classes.Status}>
                        <label>Status:</label><span>{this.props.item.status}</span>
                    </div>
                    <div className={classes.Info}>
                        <label>Location:</label><span>{this.props.item.location}</span>
                    </div>
                    <div className={classes.Info}>
                        <label>State:</label><span>{this.props.item.state}</span>
                    </div>
                    <div className={classes.Info}>
                        <label>Delivery:</label><span>{this.props.item.delivery}</span>
                    </div>
                    <div>
                        {requestButton}
                        {requests}
                    </div>

                    <div className={classes.User}>
                        {this.props.item._user.local ? (
                                [   <Thumb key="1" url={this.props.item._user.local.thumb}/>,
                                    <span key="2" className={classes.Username}>{this.props.item._user.local.username}</span>]
                            ) : null}
                        <div className={classes.Reviews}>
                            <span>{this.props.item._user.reviews ? this.props.item._user.reviews.length : 0}</span>
                            <label>Reviews</label>
                        </div>
                    </div>
                </div>
            );
        }

        if (this.state.requesting) {
            itemRequest = (
                <div className={classes.Request}>
                    <RequestForm
                        saveItem={this.saveRequestHandler}
                        cancelItem={this.cancelRequestHandler}/>
                </div>
            );
        }

        return (
            <div className={classes.ItemShare}>
                <div className={classes.Image}>
                    <img alt="Product Thumb"
                         src={this.props.item.imagesArray.length > 0 ? this.props.item.imagesArray[0].image_url : ''}/>
                </div>
                <div className={classes.Item}>
                    <h2 className={classes.Title}>{this.props.item.title}</h2>
                    {itemRequest}
                </div>
            </div>
        );
    }
}

export const mapStateToProps = state => {
    return {
        userData: state.auth.userData,
        request: state.request.request,
        loading: state.request.loading,
        error: state.request.error,
        requested: state.request.requested
    };
};

export const mapDispatchToProps = dispatch => {
    return {
        onRequestItem: (id, message) => dispatch(actions.requestItem(id, message)),
        onInitRequestItem: () => dispatch(actions.initRequestItem)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemShare);