import React, {Component} from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router'
import ItemForm from '../../../components/ItemForm/ItemForm';
import Spinner from '../../../components/UI/Spinner/Spinner';
import * as actions from '../../../store/actions/index';

import {status} from '../../../models';

import classes from './ItemNew.scss';

class ItemNew extends Component {

    saveItemHandler = (item) => {
        item.status = status.open;
        if (item.files) {
            this.props.onUploadFiles(item);
        } else {
            delete item.file;
            this.props.onNewItem(item);
        }
    };

    cancelItemHandler = () => {
        this.props.onResetItem();
        this.props.history.replace('/items');
    };

    shouldComponentUpdate = (nextProps) => {
        if(nextProps.saved){
            this.props.history.push("/");
            return false;
        }else{
            return true;
        }
    };

    render() {

        console.log(' new', this.props);

        let form = (
            <ItemForm item={this.props.item}
                      saveItem={this.saveItemHandler}
                      cancelItem={this.cancelItemHandler}/>
        );

        if (this.props.loading) {
            form = <Spinner/>;
        }

        if (this.props.error && this.props.error.message) {
            form = <span>{this.props.error.message}</span>
        }


        return (
            <div className={classes.ItemNew}>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        item: state.item.item,
        saved: state.item.saved,
        loading: state.item.loading,
        error: state.item.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onUploadFiles: item => dispatch(actions.uploadFiles(item)),
        onNewItem: item => dispatch(actions.newItem(item)),
        onResetItem: () => dispatch(actions.resetItem())
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ItemNew));