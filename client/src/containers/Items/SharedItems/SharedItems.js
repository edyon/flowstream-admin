import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Route} from 'react-router-dom';
import * as actions from '../../../store/actions/index';
import Item from '../../../components/Item/Item';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Modal from '../../../components/UI/Modal/Modal';
import ItemShare from '../ItemShare/ItemShare';
import ItemNew from '../ItemNew/ItemNew';
import ItemEdit from '../ItemEdit/ItemEdit';

import classes from './SharedItems.scss';

class Dashboard extends Component {

    state = {
        item: null
    };

    componentDidMount() {
        this.props.getSharedItems();
    }

    editItemHandler = (id) => {
        console.log('id' , id);
        this.props.history.push(`/sharedItems/edit/${id}`);
    };

    deleteItemHandler = (id) => {
        // TODO confirmation popup
        this.props.onDeleteItem(id);
    };

    openItemHandler = (item) => {
        this.setState({item: item});
    };

    closeItemHandler = () => {
        this.setState({item: null});
    };

    render() {
        let items = null;
        if (this.props.loading) {
            items = <Spinner/>
        }

        if (this.props.error && this.props.error.message) {
            items = <span className={classes.Error}>{this.props.error.message}</span>
        }

        if (this.props.items) {
            if (this.props.length === 0) {
                items = <span>no items</span>;
            } else {
                items = (
                    <ul>
                        {this.props.items.map(item => (
                            <Item key={item._id} item={item}
                                edit={true}
                                editItem={this.editItemHandler}
                                deleteItem={this.deleteItemHandler}
                                  clicked={this.openItemHandler}
                                />
                        ))}
                    </ul>
                );
            }
        }

        let openedItem = null;
        if (this.state.item) {
            openedItem = (
                <ItemShare item={this.state.item}/>
            );
        }

        return (
            <div className={classes.Dashboard}>
                <Modal show={this.state.item}
                    modalClosed={this.closeItemHandler}>
                    {openedItem}
                </Modal>
                {items}
                <Route path='/sharedItems/new' component={ItemNew} />
                <Route path='/sharedItems/edit/:id' component={ItemEdit} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        items: state.items.items,
        loading: state.items.loading,
        error: state.items.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getSharedItems: () => dispatch(actions.getItems()),
        onDeleteItem: (id) => dispatch(actions.deleteItem(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);