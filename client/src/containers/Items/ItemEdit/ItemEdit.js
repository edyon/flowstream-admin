import React, {Component} from 'react';
import {connect} from 'react-redux';

import ItemForm from '../../../components/ItemForm/ItemForm';
import Spinner from '../../../components/UI/Spinner/Spinner';
import * as actions from '../../../store/actions/index';

import classes from './ItemEdit.scss';

class ItemEdit extends Component {

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.onGetItem(this.props.match.params.id);
        }
    };

    shouldComponentUpdate = (nextProps) => {
        if(nextProps.saved){
            this.props.history.goBack();
            return false;
        }else{
            return true;
        }
    };

    saveItemHandler = (item) => {
        const changedItem = {...this.props.item, ...item};
        if (changedItem.file) {
            this.props.onUploadFiles(changedItem);
        } else {
            delete changedItem.file;
            this.props.onSaveItem(changedItem);
        }
    };

    cancelItemHandler = () => {
        this.props.onResetItem();
        //this.props.history.replace('/items');
        this.props.history.goBack();
    };

    render() {

        let form = null;
        if (this.props.loading) {
            form = <Spinner/>;
        }

        if (this.props.error && this.props.error.message) {
            form = <span>{this.props.error.message}</span>
        }

        if (this.props.item) {
            if (this.props.item._id) {
                form = <ItemForm item={this.props.item}
                    saveItem={this.saveItemHandler} cancelItem={this.cancelItemHandler} />
            }
        }

        return (
            <div className={classes.ItemEdit}>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        item: state.item.item,
        saved: state.item.saved,
        loading: state.item.loading,
        error: state.item.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onUploadFiles: file => dispatch(actions.uploadFiles(file)),
        onGetItem: id => dispatch(actions.getItem(id)),
        onSaveItem: item => dispatch(actions.editItem(item)),
        onResetItem: () => dispatch(actions.resetItem())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemEdit);