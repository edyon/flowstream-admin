import React, {Component} from 'react';
import {connect} from 'react-redux';
import Modal from '../../components/UI/Modal/Modal';
import Wrapper from '../../hoc/Wrapper/Wrapper';
import * as actions from '../../store/actions';

class ErrorHandler extends Component {

    errorConfirmedHandler = () => {
        this.props.onErrorCleared();
    };

    render() {
        return (
            <Wrapper>
                <Modal
                    show={this.props.error}
                    modalClosed={this.errorConfirmedHandler}>
                    {this.props.error ? this.props.error.message : null}
                </Modal>
            </Wrapper>
        );
    }
}

function mapStateToProps({error}) {
    return {error}
}

export default connect(mapStateToProps, actions)(ErrorHandler);