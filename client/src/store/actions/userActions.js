import axios from "axios/index";
import * as types from "./types";

export const fetchUser = () => {
    return (async (dispatch) => {
        const res = await axios.get('/api/current_user');
        dispatch({type: types.FETCH_USER, payload: res.data})
    })
};

export const handleToken = (token) => {
    return (async (dispatch) => {
        const res = await axios.post('/api/stripe', token);
        dispatch({type: types.FETCH_USER, payload: res.data})
    })
};

export const onSignUpUser = (user) => {
    return (async (dispatch) => {
        const res = await axios.post('/api/auth/local/signup',
            {
                email: user.userData.email,
                password: user.userData.password
            }
        );

        if (res.data.signUpFail) {
            dispatch({type: types.API_ERROR, payload: res.data})
        } else {
            dispatch({type: types.USER_SIGNED_UP, payload: res.data})
        }
    })
};


export const onSignInUser = (user) => {
    return ((dispatch) => {
        axios.post('/api/auth/local/signin',
            {
                email: user.userData.email,
                password: user.userData.password
            }
        ).then(res => {
            if (res.data.authFail) {
                dispatch({type: types.API_ERROR, payload: res.data})
            } else if (res.data.isNotVerified) {
                dispatch({type: types.API_ERROR, payload: res.data})
            } else {
                dispatch({type: types.USER_SIGNED_IN, payload: res.data.user})
            }
        })
    });
};

export const onForgotPasswordUser = (user) => {
    return ((dispatch) => {
        axios.post('/api/auth/local/forgotPassword',
            {
                email: user.userData.email
            }
        ).then(res => {
            if (res.data.tokenFail) {
                dispatch({type: types.API_ERROR, payload: res.data})
            } else {
                dispatch({type: types.USER_REQUEST_RESET_PASSWORD, payload: res.data})
            }
        })
    });
};

export const onResetPasswordUser = (oPassword) => {

    return ((dispatch) => {
        axios.post('/api/auth/local/resetPassword',
            {
                newPassword: oPassword.passwordData.password,
                token: oPassword.token
            }
        ).then(res => {
            if (res.data.tokenFail) {
                dispatch({type: types.API_ERROR, payload: res.data})
            } else {
                dispatch({type: types.USER_REQUEST_RESET_PASSWORD_FINISHED, payload: res.data})
            }
        })
    });
};