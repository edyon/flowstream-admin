import * as types from "./types";
import axios from "axios/index";

export const startItem = () => {
    return {
        type: types.LOADING_ITEM
    };
};

export const resetItem = () => {
    return {
        type: types.RESET_ITEM
    };
};

export const selectFilesSuccess = (imagesArray) => {
    return {
        type: types.ITEM_SELECT_FILES_SUCCESS,
        imagesArray: imagesArray
    };

};

export const uploadFileSuccess = (imagesArray) => {
    return {
        type: types.ITEM_UPLOAD_SUCCESS,
        imagesArray: imagesArray
    }
};

export const itemSaving = () => {
    return {
        type: types.ITEM_SAVING
    }
};

export const uploadFileFailed = (error) => {
    return {
        type: types.ITEM_UPLOAD_FAILED,
        error: error
    }
};

export const selectFiles = (files) => {
    return async dispatch => {

        let imagePromiseArray = [];
        for (var i = 0, len = files.length; i < len; i++) {
            let file = files[i];
            let item = {file: file};
            let pImage = selectFile(item);
            imagePromiseArray.push(pImage);
        }

        Promise.all(imagePromiseArray).then(function(values) {
            console.log(values);
        });

        Promise.all(imagePromiseArray)
            .then(function(res){
                console.log('Promise.all', res);
                dispatch(selectFilesSuccess(res));
            })
            .catch(function(err){
                console.error('err', err);
            });

    }
};


const selectFile = (item) =>{

    return new Promise((resolve, reject) => {

        let reader = new FileReader();
        let imageTitle = item.file.name;
        let imageType = item.file.type;

        reader.onload = (e) => {
            let imageSrc = e.target.result;
            let image = {
                image_title: imageTitle,
                image_type: imageType,
                image_url: imageSrc
            };

            if(imageSrc){
                resolve(image);
            }else{
                reject("boom, didn't work to select image");
            }

        };

        reader.readAsDataURL(item.file);
    })
};

export const uploadFiles = (item) => {
    return async dispatch => {

        dispatch(itemSaving());

        let imageUploadPromiseArray = [];
        for (let i = 0, len = item.files.length; i < len; i++) {
            let image = item.files[i];
            let pImageUpload = uploadFile(image);
            imageUploadPromiseArray.push(pImageUpload);
        }

        Promise.all(imageUploadPromiseArray)
            .then((res) => {
                let imagesArray = [];
                for(let i = 0; i < res.length ; i++){
                    let image = {
                        image_url: res[i].url
                    };
                    imagesArray.push(image);
                }

                dispatch(uploadFileSuccess(imagesArray));
                dispatch(saveItemAfterUpload(item, imagesArray));
            })
            .catch((err) => {
                console.error('err', err);
                dispatch(uploadFileFailed(err));
            });

    }

};


const uploadFile = (image) => {
    return new Promise((resolve, reject) => {
        axios.get(`/api/upload/sign-s3?file-name=${image.name}&file-type=${image.type}`).then(res => {
            if (res.status === 200) { // everything ok, let's upload the file
                const signedRequest = res.data.signedRequest;
                const xhr = new XMLHttpRequest();
                xhr.open('PUT', signedRequest);
                xhr.onprogress = (event) => {
                    if (event.lengthComputable) {
                        // evt.loaded the bytes the browser received
                        // evt.total the total bytes set by the header
                        // const percentComplete = (event.loaded / event.total) * 100;
                    }
                };
                xhr.onreadystatechange = () => {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(res.data);
                        } else {
                            reject("something went wrong uploading");
                        }
                    }
                };
                xhr.send(image);
            } else {
                reject("something went wrong uploading");
            }
        })
    })
};

export const saveItemAfterUpload = (item, imagesArray) => {
    return dispatch => {
        item.imagesArray  = imagesArray;
        delete item.files;
        if (item._id) {
            dispatch(editItem(item));
        } else {
            dispatch(newItem(item));
        }
    }
};

export const itemSuccess = (item) => {
    return {
        type: types.ITEM_SAVED,
        item: item
    }
};

export const itemFailed = (error) => {
    return {
        type: types.ITEM_SAVED_FAILED,
        payload: error
    }
};

export const newItem = (item) => {
    return async dispatch => {
        dispatch(startItem());

        try {
            const res = await axios.post('/api/item', item);
            dispatch(itemSuccess(res.data));
        } catch (e) {
            dispatch(itemFailed(e));
        }
    };
};

export const editItem = (item) => {
    return async dispatch => {
        dispatch(startItem());

        try {
            const res = await axios.put(`/api/item/${item._id}`, item);
            dispatch(itemSuccess(res.data));
        } catch (e) {
            dispatch(itemFailed(e));
        }
    };
};

// get item
export const itemFetched = (item) => {
    return {
        type: types.ITEM_FETCHED,
        item: item
    }
};

export const itemFetchedFailed = (error) => {
    return {
        type: types.ITEM_FETCHED_FAILED,
        payload: error
    }
};

export const getItem = (id) => {
    return async dispatch => {
        try {
            const res = await axios.get(`/api/item/${id}`);
            const item = res.data && res.data.length > 0 ? res.data[0] : null;
            dispatch(itemFetched(item));
        } catch (e) {
            dispatch(itemFetchedFailed(e));
        }
    };
};

// delete item
export const itemDeleted = (id) => {
    return {
        type: types.ITEM_DELETED,
        id: id
    }
};

export const itemDeletedFailed = (error) => {
    return {
        type: types.ITEM_DELETED_FAILED,
        error: error
    }
};

export const deleteItem = (id) => {
    return async dispatch => {
        try {
            await axios.delete(`/api/item/${id}`);
            dispatch(itemDeleted(id));
        } catch (e) {
            dispatch(itemDeletedFailed(e));
        }
    };
};
