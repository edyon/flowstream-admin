import * as types from "./types";
import axios from "axios/index";

export const startFetchingItems = () => {
    return {
        type: types.START_FETCHING_ITEMS
    };
};

export const itemsFetched = (items) => {
    return {
        type: types.ITEMS_FETCHED,
        items: items
    }
};

export const itemsFetchedFailed = (error) => {
    return {
        type: types.ITEMS_FETCHED_FAILED,
        error: error
    }
};

export const getItems = () => {
    return async dispatch => {
        dispatch(startFetchingItems());

        try {
            const res = await axios.get('/api/items');
            dispatch(itemsFetched(res.data));
        } catch (e) {
            dispatch(itemsFetchedFailed(e));
        }
    };
};

export const getAllItems = () => {
    return async dispatch => {
        dispatch(startFetchingItems());
        
        try {
            const res = await axios.get('/api/items/all');
            dispatch(itemsFetched(res.data));
        } catch (e) {
            dispatch(itemsFetchedFailed(e));
        }
    };
};
