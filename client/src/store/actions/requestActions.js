import axios from "axios/index";
import * as types from "./types";

export const initRequestItem = () => {
    return {
        type: types.INIT_REQUEST_ITEM
    }
};

export const startRequestingItem = () => {
    return {
        type: types.START_REQUESTING_ITEM
    }
};

export const itemRequested = (request) => {
    return {
        type: types.ITEM_REQUESTED,
        request: request
    }
};

export const itemRequestedFailed = (error) => {
    return {
        type: types.ITEM_REQUESTED_FAILED,
        error: error
    }
};

export const requestItem = (id, request) => {
    return async dispatch => {
        dispatch(startRequestingItem());

        try {
            const res = await axios.put(`/api/item/${id}/request`, request);
            dispatch(itemRequested(res.data));
        } catch (e) {
            dispatch(itemRequestedFailed(e));
        }
    };
};