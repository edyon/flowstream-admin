import * as types from './types';

export const onErrorCleared = () => {
    return ({
        type: types.API_ERROR_CLEARED
    })
};