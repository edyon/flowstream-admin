import * as types from '../actions/types';

const initial_state = {
    request: null,
    loading: false,
    error: false,
    requested: false
};

const requestReducer = (state = initial_state, action) => {
    switch (action.type) {
        case types.INIT_REQUEST_ITEM:
            return {
                request: null,
                loading: false,
                error: false,
                requested: false
            };
        case types.START_REQUESTING_ITEM:
            return {
                request: null,
                loading: true,
                error: false,
                requested: false
            };
        case types.ITEM_REQUESTED:
            return {
                request: action.request,
                loading: false,
                error: false,
                requested: true
            };
        case types.ITEM_REQUESTED_FAILED:
            return {
                request: null,
                loading: false,
                error: action.error,
                requested: false
            };
        default:
            return state;
    }
};

export default requestReducer;