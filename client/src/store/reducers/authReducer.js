import {
    FETCH_USER,
    USER_SIGNED_IN,
    USER_SIGNED_UP,
    USER_REQUEST_RESET_PASSWORD_INIT,
    USER_REQUEST_RESET_PASSWORD,
    USER_REQUEST_RESET_PASSWORD_FINISHED} from '../actions/types';

const initial_state = {
    userData: {},
    user_signed_in: false,
    user_signed_up: false,
    user_request_reset_password: false,
    user_reset_password_finished: false
};

const authReducer = (state = initial_state, action) => {
    switch(action.type){
        case FETCH_USER:
            return {
                ...state,
                userData: action.payload || null,
            };
        case USER_SIGNED_IN:
            return {
                ...state,
                user_signed_in: true,
                userData: action.payload
            };
        case USER_SIGNED_UP:
            return {
                ...state,
                user_signed_up: true
            };
        case USER_REQUEST_RESET_PASSWORD_INIT:
            return {
                ...state,
                user_request_reset_password: false
            };
        case USER_REQUEST_RESET_PASSWORD:
            return {
                ...state,
                user_request_reset_password: true
            };
        case USER_REQUEST_RESET_PASSWORD_FINISHED:
            return {
                ...state,
                user_request_reset_password: false,
                user_reset_password_finished: true
            };
        default:
            return state
    }
};

export default authReducer;

