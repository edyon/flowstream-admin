import { API_ERROR, API_ERROR_CLEARED } from '../actions/types';

const initial_state = null;

const errorReducer = (state = initial_state, action) => {
    switch(action.type){
        case API_ERROR:
            return action.payload;
        case API_ERROR_CLEARED:
            return null;
        default:
            return state
    }
};

export default errorReducer;