import { combineReducers } from 'redux';

import authReducer from './authReducer';
import errorReducer from './errorReducer';
import itemReducer from './itemReducer';
import itemsReducer from './itemsReducer';
import requestReducer from './requestReducer';

export default combineReducers({
    auth : authReducer,
    error: errorReducer,
    item: itemReducer,
    items: itemsReducer,
    request: requestReducer
});