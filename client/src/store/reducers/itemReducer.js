import * as types from '../actions/types';

const initial_state = {
    item: null,
    loading: false,
    error: false,
    saved: false
};

const itemReducer = (state = initial_state, action) => {
    switch (action.type) {
        case types.RESET_ITEM:
            return {
                item: null,
                loading: false,
                error: false
            };
        case types.LOADING_ITEM:
            return {
                loading: true,
                error: false
            };
        case types.ITEM_SELECT_FILES_SUCCESS:
            return {
                item: {
                    ...state.item,
                    imagesArray: action.imagesArray
                },
                loading: false,
                error: false
            };
        case types.ITEM_SAVING:
            return {
                item: {
                    ...state.item
                },
                loading: true,
                error: false
            };
        case types.ITEM_UPLOAD_SUCCESS:
            return {
                item: {
                    ...state.item,
                    imagesArray: action.imagesArray
                },
                loading: false,
                error: false
            };
        case types.ITEM_UPLOAD_FAILED:
            const errorMessage = 'Upload failed, please try again';
            return {
                item: null,
                loading: false,
                error: {
                    message: errorMessage
                }
            };
        case types.ITEM_SAVED:

            return {
                item: null,
                loading: false,
                error: false,
                saved: true
            };
        case types.ITEM_FETCHED:
            return {
                item: {
                    ...action.item
                },
                loading: false,
                error: false
            };
        case types.ITEM_SAVED_FAILED:
        case types.ITEM_FETCHED_FAILED:
            return {
                item: null,
                loading: false,
                error: action.error
            };
        default:
            return state;
    }
};

export default itemReducer;