import authReducer from './authReducer';
import * as actionTypes from '../actions/types';

describe('auth reducer', () => {
    it('should return initial state', () =>  {
        expect(authReducer(undefined, {})).toEqual({
            userData: {},
            user_signed_in: false,
            user_signed_up: false,
            user_request_reset_password: false,
            user_reset_password_finished: false
        })
    });

    it('should store userdata and set user_signed_in value to true', () => {
        expect(authReducer({
            userData: {},
            user_signed_in: false,
            user_signed_up: false,
            user_request_reset_password: false,
            user_reset_password_finished: false
        }, {
            type: actionTypes.USER_SIGNED_IN,
            payload: {
                name: 'some-name'
            }
        })).toEqual({
            userData: {
                name: 'some-name'
            },
            user_signed_in: true,
            user_signed_up: false,
            user_request_reset_password: false,
            user_reset_password_finished: false
        })
    });

    it('should set user_signed_up value to true if user signed up', () => {
        expect(authReducer({
            userData: {
                name: 'some-name'
            },
            user_signed_in: false,
            user_signed_up: false,
            user_request_reset_password: false,
            user_reset_password_finished: false
        }, {
            type: actionTypes.USER_SIGNED_UP,
            payload: null
        })).toEqual({
            userData: {
                name: 'some-name'
            },
            user_signed_in: false,
            user_signed_up: true,
            user_request_reset_password: false,
            user_reset_password_finished: false
        })
    });

    it('should clear userdata when user is signed out', () => {
        expect(authReducer({
            userData: {},
            user_signed_in: false,
            user_signed_up: false,
            user_request_reset_password: false,
            user_reset_password_finished: false
        }, {
            type: actionTypes.USER_SIGNED_IN,
            payload: null
        })).toEqual({
            userData: null,
            user_signed_in: true,
            user_signed_up: false,
            user_request_reset_password: false,
            user_reset_password_finished: false
        })
    });
});




