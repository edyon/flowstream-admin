import * as types from '../actions/types';

const initial_state = {
    items: null,
    loading: false,
    error: false
};

const itemsReducer = (state = initial_state, action) => {
    switch (action.type) {
        case types.START_FETCHING_ITEMS:
            return {
                items: null,
                loading: true,
                error: false
            };
        case types.ITEMS_FETCHED:
            return {
                items: action.items,
                loading: false,
                error: false
            };
        case types.ITEMS_FETCHED_FAILED:
            return {
                items: null,
                loading: false,
                error: action.error
            };
        case types.ITEM_DELETED:
            const items = [...state.items];
            const item = items.find(item => action.id === item._id);
            const index = items.indexOf(item);
            if (index !== -1) items.splice(index, 1);
            return {
                ...state,
                items: items,
            };
        case types.ITEM_SAVED:
            let newItems = [...state.items];
            let newItem = newItems.find(i => action.item._id === i._id);
            let newIndex = newItems.indexOf(newItem);
            if (newIndex !== -1) {
                newItems[newIndex] = {
                    ...action.item,
                    _user: {
                        ...newItem._user
                    }
                }
            }

            return {
                ...state,
                items: newItems,
            };
        default:
            return state;
    }
};

export default itemsReducer;