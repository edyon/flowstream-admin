import React, {Component} from 'react';
import {BrowserRouter, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from './store/actions';

import Header from './containers/Header/Header';
import ErrorHandler from './containers/ErrorHandler/ErrorHandler';
import ItemsOverview from './containers/Items/ItemsOverview/ItemsOverview';
import SharedItems from './containers/Items/SharedItems/SharedItems';
import verifyUser from './components/StaticInfoPages/VerifyUser';
import PrivateRoute from './containers/PrivateRoute/PrivateRoute';
import User from './containers/User/User';

import classes from './App.scss';

class App extends Component {

    componentDidMount() {
        this.props.fetchUser();
    }

    render() {
        return (
                <div className={classes.windowContainer}>
                    <BrowserRouter>
                        <div>
                            <Header/>
                            <Route path="/" exact render={() => (
                                <Redirect to="/items"/>
                            )}/>
                            <Route path="/items" component={ItemsOverview}/>
                            <Route path="/verifyUser" exact component={verifyUser}/>
                            <Route path="/user" component={User}/>
                            <PrivateRoute authenticated={(!this.props.auth) ? false : true } path="/sharedItems" component={SharedItems}/>
                            <ErrorHandler/>
                        </div>
                    </BrowserRouter>
                </div>
        )
    }
}

function mapStateToProps({auth}) {
    return {auth}
}

export default connect(mapStateToProps, actions)(App);