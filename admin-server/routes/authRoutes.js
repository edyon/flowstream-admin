const nodemailer = require('nodemailer');
const passport = require('passport');
const mongoose = require('mongoose');
const crypto = require('crypto');
const User = mongoose.model('User');
const Token = mongoose.model('Token');
const keys = require('../config/keys');


module.exports = (app) => {

    app.post('/api/auth/local/signin',
        (req, res, next) => {
            passport.authenticate('local-signin', function (err, user, info) {
                if (!user) {
                    if (info.wrongPassword) {
                        return res.status(401).json(info);
                    }
                    return res.status(200).json(info);
                } else {
                    req.logIn(user, function (err) {
                        if (err) {
                            return next(err);
                        }

                        if (user.isVerified === false) {
                            return res.send({isNotVerified: true, message: 'this account needs to be verified first.'});
                        }

                        return res.status(200).json({user});
                    });
                }

            })(req, res, next);
        }
    );

    app.get('/api/logout', (req, res) => {
        req.logout();
        res.send({isLoggedOut: true})
    });

    app.get('/api/current_user', (req, res) => {
        res.send(req.user);
    });


};