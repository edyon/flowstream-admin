const mongoose = require('mongoose');
const Item = mongoose.model('Item');
const Request = mongoose.model('Request');
const requireLogin = require('../middlewares/requireLogin');

module.exports = (app) => {
    app.post('/api/item', requireLogin, async (req, res) => {
        try {
            const item = await new Item({
                _id: new mongoose.Types.ObjectId(),
                _user: req.user._id,
                title: req.body.title,
                description: req.body.description,
                imagesArray: req.body.imagesArray,
                category: req.body.category,
                sub_category: req.body.sub_category,
                location: req.body.location,
                price: req.body.price,
                originalPrice: req.body.originalPrice,
                state: req.body.state,
                obtainMethod: req.body.obtainMethod,
                delivery: req.body.delivery,
                payment: req.body.payment,
                status: req.body.status,
                active: false
            }).save();

            res.send(item);
        } catch (e) {
            res.send(500, {error: e});
        }
    });

    app.put('/api/item/:id', requireLogin, async (req, res) => {
        try {
            const item = await Item.findOneAndUpdate({_id: req.params.id}, req.body, {new: true});
            res.status(200).send(item);
        } catch (e) {
            res.status(500).send({error: e});
        }
    });

    app.get('/api/item/:id', requireLogin, async (req, res) => {
        try {
            const item = await Item.find({_id: req.params.id});
            res.status(200).send(item);
        } catch (e) {
            res.status(500).send({error: e});
        }
    });

    app.delete('/api/item/:id', requireLogin, async (req, res) => {
        try {
            await Item.findByIdAndRemove({_id: req.params.id});
            res.status(200).send({result: 'Removed item'});
        } catch (e) {
            res.status(500).send({error: e});
        }
    });

    // add request to item
    app.put('/api/item/:id/request', requireLogin, async (req, res) => {
        try {
            const request = await new Request({
                _user: req.user._id,
                message: req.body.message
            });

            const item = await Item.findById({_id: req.params.id});
            const requests = item.requests;
            requests.push(request);

            item.set({requests: requests});
            item.save();
            res.status(200).send(request);
        } catch (e) {
            res.status(500).send({error: e});
        }
    });

    // items
    app.get('/api/items', requireLogin, async (req, res) => {
        try {
            const items = await Item.find({_user: req.user.id}).populate('_user');
            res.status(200).send(items);
        } catch (e) {
            res.status(500).send({error: e});
        }
    });

    app.get('/api/items/all', async (req, res) => {
        try {
            let items;

            items = await Item.find().populate('_user');

            res.status(200).send(items);
        } catch (e) {
            console.log(e);
            res.status(500).send({error: e});
        }
    });

};