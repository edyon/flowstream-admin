const mongoose = require('mongoose');
const User = mongoose.model('User');
const Request = mongoose.model('Request');
const requireLogin = require('../middlewares/requireLogin');

module.exports = (app) => {
    app.get('/api/users/all', requireLogin, async (req, res) => {
        try {
            const users = await User.find();
            res.status(200).send(users);
        } catch (e) {
            console.log(e);
            res.status(500).send({error: e});
        }
    });

};