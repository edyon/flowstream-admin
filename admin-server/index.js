const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const aws = require('aws-sdk');
const keys = require('./config/keys');
require('./models/User');
require('./models/Token');
require('./models/Item');
require('./models/Request');
require('./models/Review');
require('./services/passport');
const authRoutes = require('./routes/authRoutes');
const billingRoutes = require('./routes/billingRoutes');
const itemRoutes = require('./routes/itemRoutes');
const uploadRoutes = require('./routes/uploadRoutes');
const userRoutes = require('./routes/userRoutes');

mongoose.connect(keys.mongoURI);

const app = express();

const S3_BUCKET = keys.S3_BUCKET;
aws.config = {
    region: 'ap-southeast-2',
    secretAccessKey: keys.AWS_SECRET_ACCESS_KEY,
    accessKeyId: keys.AWS_ACCESS_KEY_ID,
};

app.use(bodyParser.json());

app.use(
    cookieSession({
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [keys.cookieKey]
    })
);

app.use(passport.initialize());
app.use(passport.session());

authRoutes(app);
billingRoutes(app);
itemRoutes(app);
userRoutes(app);
uploadRoutes(app, S3_BUCKET, aws);
//another way of writing this would be: require('./routes/authRoutes')(app)
// the require function returns a function that we then immediately call with the app as variable


if (process.env.NODE_ENV === 'production') {
    // Express will serve up production assets like our main.js file
    app.use(express.static('client/build'))

    // Express will serve up the index.html file when it doesn't recognise the route

    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    })
}

const PORT = process.env.PORT || 6000;

app.listen(PORT);



