const mongoose = require('mongoose');
const {Schema} = mongoose;

const itemSchema = new Schema({
    _user: {type: Schema.Types.ObjectId, ref: 'User'},
    title: String,
    description: String,
    imagesArray: [{image_url: String}],
    location: String,
    price: String,
    originalPrice: String,
    state: String,
    shareMethod: Number,
    obtainMethod: Number,
    shipping: [],
    payment: [],
    category: String,
    sub_category: String,
    status: String,
    active: false,
    dateCreated: {type: Date, default: Date.now},
    lastResponded: Date,
    requests: []
});

mongoose.model('Item', itemSchema);