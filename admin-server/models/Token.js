const mongoose = require('mongoose');
const { Schema } = mongoose;


const tokenSchema = new Schema({
    _userId: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    token: { type: String, required: true},
    createdAt: { type: Date, expires: 86400, default: Date.now },
    tokenType: String
});



mongoose.model('Token', tokenSchema);