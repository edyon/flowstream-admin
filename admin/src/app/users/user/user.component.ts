import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";

import {User} from "../../interfaces/user";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  selectedUser: User;

  constructor(private store: Store<{user: {}}>) { }

  ngOnInit() {
    this.store.select('users')
        .subscribe((users: any) => {
          this.selectedUser = users.selectedUser;
        });
  }

}
