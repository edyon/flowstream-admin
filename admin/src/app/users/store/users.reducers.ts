import * as UsersActions from './users.actions';
import {User} from '../../interfaces/user';

export interface State {
    users: Array<User>;
    selectedUser: User;
}

const initialState: State = {
    users: [],
    selectedUser : null
};

export function usersReducer (state = initialState, action: UsersActions.UsersActions): {} {

    switch (action.type) {
        case UsersActions.SET_USERS:
          console.log("set users ", action.payload);
            return {
                ...state,
                users: [
                    ...action.payload
                ]
            };
        case UsersActions.SELECT_USER:
            return {
                ...state,
                users: [...state.users],
                selectedUser: {
                    ...action.payload
                }
            };
        default:
            return state;
    }
}

