import { Action } from '@ngrx/store';
import {User} from "../../interfaces/user";

export const GET_USERS = 'GET_USERS';
export const SELECT_USER = 'SELECT_USER';
export const SET_USERS = 'SET_USERS';

export class GetUsers implements Action {
    readonly type = GET_USERS;
}

export class SelectUser implements Action {
    readonly type = SELECT_USER;
    constructor(public payload: User) {};
}

export class SetUsers implements Action {
    readonly type = SET_USERS;
    constructor(public payload: User[]) {};
}

export type UsersActions = SelectUser | GetUsers | SetUsers
;
