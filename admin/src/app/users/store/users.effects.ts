import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, Effect} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';

import * as UsersActions from './users.actions';

@Injectable()
export class UsersEffects {
    @Effect()
    getUsers = this.actions$
        .ofType(UsersActions.GET_USERS)
        .pipe(switchMap((action: UsersActions.GetUsers) => {
                return this.http.get('/api/users/all', {
                    observe: 'body',
                    responseType: 'json'
                })
            }),
            map(
                (users) => {
                    return {
                        type: UsersActions.SET_USERS,
                        payload: users
                    };
                })
        )


    constructor(private actions$: Actions, private http: HttpClient) {
    }
}

