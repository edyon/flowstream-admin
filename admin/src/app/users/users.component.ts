import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import { Store } from '@ngrx/store';

import {DataStorageService} from "../shared/data-storage.service";
import {User} from "../interfaces/user";
import * as UsersActions from "./store/users.actions";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  displayedColumns = ['username', 'email'];
  users: User[] = [];
  dataSource: MatTableDataSource<User>;
  userShow: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dataStorageService: DataStorageService, private store: Store<{users: User[]}>) { }

  ngOnInit() {

    this.store.select('users')
      .subscribe((response: any) => {
        this.users = response.users;
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

    this.store.dispatch( new UsersActions.GetUsers());

  }

  onUserSelect(user: User) {
    this.userShow = true;
    this.store.dispatch(new UsersActions.SelectUser(user));
  }

}
