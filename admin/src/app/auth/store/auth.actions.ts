import {Action} from '@ngrx/store';
import {User} from '../../interfaces/user';
export const USER_SIGNIN = 'USER_SIGNIN';
export const USER_SIGNOUT = 'USER_SIGNOUT';
export const SET_USER = 'SET_USER';
export const GET_USER = 'GET_USER';
export const USER_SIGNED_OUT = 'USER_SIGNED_OUT';
export const USER_SIGNIN_FAIL = 'USER_SIGNIN_FAIL';

export class UserSignin implements Action {
    readonly type = USER_SIGNIN;
    constructor(public payload: User) {}
}

export class UserSigninFail implements Action {
    readonly type = USER_SIGNIN_FAIL;
}

export class UserSignout implements Action {
    readonly type = USER_SIGNOUT;
}

export class SetUser implements Action {
    readonly type = SET_USER;
    constructor( public payload: User) {}
}

export class GetUser implements Action {
    readonly type = GET_USER;
}

export class UserSignedOut implements Action {
    readonly type = USER_SIGNED_OUT;
}

export type AuthActions = UserSignin | UserSigninFail | UserSignout | SetUser | GetUser | UserSignedOut
;
