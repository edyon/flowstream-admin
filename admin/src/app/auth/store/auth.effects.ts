import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as AuthActions from './auth.actions';
import {HttpClient} from '@angular/common/http';
import {catchError, filter, map, switchMap, take, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';

@Injectable()
export class AuthEffects {
    constructor(private actions$: Actions, private http: HttpClient, private router: Router, private store: Store<{ auth: {} }>) {
    }

    @Effect()
    signInUser = this.actions$
        .ofType(AuthActions.USER_SIGNIN)
        .pipe(switchMap((action: AuthActions.UserSignin) => {
                return this.http.post('/api/auth/local/signin', action.payload, {
                    observe: 'body',
                    responseType: 'json'
                })
                .pipe(
                    map((res: any) => {
                        if (!res.authFail) {
                            {
                                this.router.navigate(['/items']);
                                return {
                                    type: AuthActions.SET_USER,
                                    payload: res.user
                                };
                            }
                        } else {
                            return {
                                type: AuthActions.USER_SIGNIN_FAIL
                            };
                        }
                    })
                    , catchError((err) => {
                        console.log('catch error ', err);
                        return of({
                            type: AuthActions.USER_SIGNIN_FAIL
                        });
                    })
                );
            })
        );

    @Effect()
    getUser = this.actions$
        .ofType(AuthActions.GET_USER)
        .pipe(switchMap((action: AuthActions.GetUser) => {
                return this.http.get('/api/current_user', {
                    observe: 'body',
                    responseType: 'json'
                });
            }),
            take(1),
            map((result) => {
                return {
                    type: AuthActions.SET_USER,
                    payload: result !== null ? result : null
                };
            })
        );
}


