import * as AuthActions from './auth.actions';

export interface State {
    user: {};
    authenticated: boolean;
}

const initialState: State = {
    user: {},
    authenticated: false
};

export function authReducer(state = initialState, action: AuthActions.AuthActions) {
    switch (action.type) {
        case AuthActions.SET_USER:
            return {
                ...action.payload,
                authenticated: true
            };
        case AuthActions.USER_SIGNIN_FAIL:
            return {
                ...state,
                authenticated: false
            };
        case AuthActions.USER_SIGNOUT:
            return{
                ...state,
                authenticated: false
            };
        default:
            return state;
    }
}
