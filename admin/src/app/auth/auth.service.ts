import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import * as AuthActions from './store/auth.actions';
import {UserSignout} from './store/auth.actions';
import {Store} from '@ngrx/store';

@Injectable()
export class AuthService {

    user: {} = null;

    constructor(private http: HttpClient, private router: Router, private store: Store<{auth: {}}>) {
    }

    onSignInUser(user) {
        return this.http.post('/api/auth/local/signin', user, {
            observe: 'body',
            responseType: 'json'
        });
    }

    getCurrentUser() {
        return this.http.get('/api/current_user', {
            observe: 'body',
            responseType: 'json'
        });
    }

    setUser(user) {
        this.user = user;
    }

    logout() {
        this.http.get('/api/logout')
            .subscribe((res: any) => {
                if (res.isLoggedOut) {
                    console.log('user signed out');
                    this.store.dispatch(new AuthActions.UserSignout());
                    this.router.navigate(['/signin']);
                }
            });
    }

    isAuthenticated() {
        return this.user != null;
    }

}
