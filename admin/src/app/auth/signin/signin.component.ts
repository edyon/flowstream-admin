import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import {AuthService} from '../auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import {User} from '../../interfaces/user';
import {Store} from '@ngrx/store';
import * as AuthActions from '../store/auth.actions';



@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

    hide = true;
    signinForm: FormGroup;
    user: User = null;
    path = '';

    constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private store: Store<any>) {

    }

    ngOnInit() {
        this.signinForm = new FormGroup({
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null, Validators.required)
        });
    }

    onSubmit(form: NgForm) {

        const aUser = <User>{
            ...form.value
        };

        console.log(aUser);
        this.store.dispatch(new AuthActions.UserSignin(aUser));

        // this.authService.onSignInUser(aUser)
        //     .subscribe((res: any) => {
        //         if (res.authFail) {
        //         } else if (res.isNotVerified) {
        //         } else {
        //             this.authService.setUser(res.user);
        //             this.router.navigate(['/items'])
        //         }
        //     });


    }



}
