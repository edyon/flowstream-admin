import {Injectable} from '@angular/core';
import {
    Router, ActivatedRouteSnapshot,
    RouterStateSnapshot, CanActivate
} from '@angular/router';

import {AuthService} from './auth.service';
import {Observable} from 'rxjs';
import {filter, switchMap, tap} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import * as AuthActions from './store/auth.actions';
import * as fromAuth from './store/auth.reducer';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs/internal/observable/of';
import {User} from '../interfaces/user';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router, private http: HttpClient, private store: Store<{ auth: {} }>) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        return this.getFromStoreOrApi()
            .pipe(switchMap((result) => {
                if (result) {
                    return of(true);
                }
            }));
    }

    getFromStoreOrApi(): Observable<User> {
        return this.store.select('auth')
            .pipe(
                tap((authState: fromAuth.State) => {
                    if (authState.authenticated) {
                        return authState;
                    } else {
                        this.store.dispatch(new AuthActions.GetUser());
                    }
                }),
                filter((result: any) => {
                    return result.authenticated;
                }),
                tap((result) => {
                    if (result && result.isAdmin) {
                        return result;
                    } else {
                        this.router.navigate(['/signin']);
                    }
                })
            );
    }

}
