import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class DataStorageService {
    constructor(private http: HttpClient) {};

  getUsers() {
    return this.http.get('/api/users/all', {
      observe: 'body',
      responseType: 'json'
    })
  }

    getItems() {
        return this.http.get('/api/items/all', {
            observe: 'body',
            responseType: 'json'
        })
    }
}
