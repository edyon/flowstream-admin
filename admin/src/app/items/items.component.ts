import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import {DataStorageService} from '../shared/data-storage.service';
import { Store } from '@ngrx/store';
import * as ItemsActions from './store/items.actions';
import {Item} from '../interfaces/item';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  displayedColumns = ['title', 'description', 'active', 'category'];
  items: Item[] = [];
  dataSource: MatTableDataSource<Item>;
  itemShow: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private dataStorageService: DataStorageService, private store: Store<{items: Item[]}>) {}

  ngOnInit() {

      this.store.select('items')
          .subscribe((response: any) => {
              this.items = response.items;
              this.dataSource = new MatTableDataSource<Item>(this.items);
              this.dataSource.sort = this.sort;
              this.dataSource.paginator = this.paginator;
          });

      this.store.dispatch( new ItemsActions.GetItems());

  }

  onItemSelect(item: Item) {
      this.itemShow = true;
      this.store.dispatch(new ItemsActions.SelectItem(item));
  }
}




