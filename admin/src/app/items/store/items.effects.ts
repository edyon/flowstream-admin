import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as ItemsActions from './items.actions';
import {map, switchMap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ItemsEffects {
    @Effect()
    getItems = this.actions$
        .ofType(ItemsActions.GET_ITEMS)
        .pipe(switchMap((action: ItemsActions.GetItems) => {
                return this.http.get('/api/items/all', {
                    observe: 'body',
                    responseType: 'json'
                })
            }),
            map(
                (items) => {
                    return {
                        type: ItemsActions.SET_ITEMS,
                        payload: items
                    };
                })
        )


    constructor(private actions$: Actions, private http: HttpClient) {
    }
}

