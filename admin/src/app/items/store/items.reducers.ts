import * as ItemsActions from './items.actions';
import {Item} from '../../interfaces/item';

export interface State {
    items: Array<Item>;
    selectedItem: Item;
}

const initialState: State = {
    items: [],
    selectedItem : null
};

export function itemsReducer (state = initialState, action: ItemsActions.ItemsActions): {} {

    switch (action.type) {
        case ItemsActions.SET_ITEMS:
            return {
                ...state,
                items: [
                    ...action.payload
                ]
            };
        case ItemsActions.SELECT_ITEM:
            return {
                ...state,
                items: [...state.items],
                selectedItem: {
                    ...action.payload
                }
            };
        default:
            return state;
    }
}

