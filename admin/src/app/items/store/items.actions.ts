import { Action } from '@ngrx/store';
import {Item} from '../../interfaces/item';

export const GET_ITEMS = 'GET_ITEMS';
export const SELECT_ITEM = 'SELECT_ITEM';
export const SET_ITEMS = 'SET_ITEMS';

export class GetItems implements Action {
    readonly type = GET_ITEMS;
}

export class SelectItem implements Action {
    readonly type = SELECT_ITEM;
    constructor(public payload: Item) {};
}

export class SetItems implements Action {
    readonly type = SET_ITEMS;
    constructor(public payload: Item[]) {};
}

export type ItemsActions = SelectItem | GetItems | SetItems
;