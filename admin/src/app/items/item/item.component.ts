import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {Item} from "../../interfaces/item";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  selectedItem: Item;

  constructor(private store: Store<{item: {}}>) { }

  ngOnInit() {
    this.store.select('items')
        .subscribe((items: any) => {
          this.selectedItem = items.selectedItem;
        });
  }

}
