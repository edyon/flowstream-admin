import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {Routes, RouterModule} from "@angular/router";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ReactiveFormsModule} from "@angular/forms";
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from "@ngrx/effects";
import {
MatButtonModule,
MatToolbarModule,
MatCardModule,
MatFormFieldModule,
MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule
} from '@angular/material';

import {AppComponent} from './app.component';
import {SigninComponent} from './auth/signin/signin.component';
import { HomeComponent } from './core/home/home.component';
import { UsersComponent } from './users/users.component';
import { ItemsComponent } from './items/items.component';
import { HeaderComponent } from './core/header/header.component';
import { SignoutComponent } from './auth/signout/signout.component';
import {DataStorageService} from "./shared/data-storage.service";
import {AuthService} from "./auth/auth.service";
import {AuthGuard} from "./auth/auth-guard.service";
import {itemsReducer} from "./items/store/items.reducers";
import {usersReducer} from "./users/store/users.reducers";
import { ItemComponent } from './items/item/item.component';
import { UserComponent } from './users/user/user.component';
import {ItemsEffects} from "./items/store/items.effects";
import {AuthEffects} from './auth/store/auth.effects';
import {authReducer} from './auth/store/auth.reducer';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import {UsersEffects} from "./users/store/users.effects";

const appRoutes: Routes = [
    {path: '', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
    {path: 'items', component: ItemsComponent, canActivate: [AuthGuard]},
    {path: 'signin', component: SigninComponent},
    {path: '**' , redirectTo:'/signin'}
];


@NgModule({
    declarations: [
        AppComponent,
        SigninComponent,
        HomeComponent,
        UsersComponent,
        ItemsComponent,
        HeaderComponent,
        SignoutComponent,
        ItemComponent,
      UserComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        HttpClientModule,
        MatButtonModule,
        MatToolbarModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({items: itemsReducer, auth: authReducer, users: usersReducer}),
        EffectsModule.forRoot([ItemsEffects, AuthEffects, UsersEffects]),
        !environment.production ? StoreDevtoolsModule.instrument() : []
    ],
    providers: [DataStorageService, AuthService, AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
