export interface User {
    credits: {
        type: Number,
        default: 0
    },
    local: {
        email: String,
        password: String,
        username: String
    },
    google: {
        googleId: String
    },
    isVerified: Boolean,
    forgotPasswordToken: String,
    reviews: [{}],
    isAdmin: {
        type: Boolean,
        default: false
    }
}