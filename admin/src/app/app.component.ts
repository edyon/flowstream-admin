import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, Params, RouterEvent} from "@angular/router";
import {Store} from '@ngrx/store';
import * as AuthActions from './auth/store/auth.actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Cycly admin';
    isAuth: boolean;

    constructor(private route: ActivatedRoute, private router: Router, private store: Store<{ auth: {} }>) {

    }

    ngOnInit() {
        this.router.events
            .subscribe(
                (params: RouterEvent) => {
                    this.isAuth = params.url !== '/signin' ? true : false ;
                }
            );
    }




}
