const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');
const User = mongoose.model('User');


//user here is what came back from the db
//this is used when logging in, after the user has been retrieved from the db. After this the cookie will be set
passport.serializeUser((user, done) => {
    // null here is an error object, but we don't define it
    // the user id is the id from the user from the db, which is not the googleId
    done(null, user.id)
});

// this is called when for example a call is made to the db, get some posts or so. the cookie is send,
// and from that encoded cookie a user is retrieved (magic!, hence the key), after that it is turned into a user model, bam!
// user is known, fancy shanzy, do something
passport.deserializeUser((id, done) => {
    User.findById(id)
        .then((userModel) => {
            done(null, userModel);
        })
});


//      Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(
    new GoogleStrategy({
        clientID: keys.googleClientID,
        clientSecret: keys.googleClientSecret,
        callbackURL: '/auth/google/callback',
        proxy: true
        //proxy: true needed for deploying on heroku on prod, since it acts as a proxy, and google auth callback will fail. other option is to                       //add a full callback url path instead of relative
    }, async(accessToken, refreshToken, profile, done) => {

        const existingUser = await User.findOne({'google.googleId': profile.id});

        if (existingUser) {
            //user exists!
            done(null, existingUser);
        } else {
            //we don't have that user, save the user
            const newUser = await new User();
            newUser.google.googleId = profile.id
            newUser.save();
            done(null, newUser);
        }

    })
);


passport.use('local-signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback : true
    }, (req, username, password, done) => {

        User.findOne({ 'local.email': username}, (err, user) => {

            if (err)
                return done(err);

            if (!user)
                return done(null, false, { authFail : true, wrongPassword: false, message: 'Incorrect username.' });

            if (!user.validPassword(password)){
                return done(null, false, { authFail : true, wrongPassword: true, message: 'Incorrect password.' });
            }

            return done(null, user);

        });
    })
);



