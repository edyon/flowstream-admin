const mongoose = require('mongoose');
const {Schema} = mongoose;

const requestSchema = new Schema({
    _user: {type: Schema.Types.ObjectId, ref: 'User'},
    message: String,
    dateCreated: {type: Date, default: Date.now}
});

mongoose.model('Request', requestSchema);