const mongoose = require('mongoose');
const {Schema} = mongoose;

const reviewSchema = new Schema({
    _user: {type: Schema.Types.ObjectId, ref: 'User'},
    message: String,
    dateCreated: {type: Date, default: Date.now}
});

mongoose.model('Review', reviewSchema);