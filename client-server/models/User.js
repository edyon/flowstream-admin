const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const {Schema} = mongoose;

const userSchema = new Schema({
    credits: {
        type: Number,
        default: 0
    },
    local: {
        email: String,
        password: String,
        username: String
    },
    google: {
        googleId: String
    },
    isVerified: Boolean,
    forgotPasswordToken: String,
    reviews: [{type: Schema.Types.ObjectId, ref: 'Review'}]
});


// generating a hash
userSchema.methods.generatePasswordHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.generateForgotPasswordToken = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

mongoose.model('User', userSchema);